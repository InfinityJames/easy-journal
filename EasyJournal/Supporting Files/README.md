## Easy Journal

An app which a person can use to easily log there daily activities. Keeping diaries can be difficult, but with the ability to just quickly log what you've been up to, you can keep track of your life without the effort of writing a journal post.
