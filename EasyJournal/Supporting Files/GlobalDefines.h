//
//  Header.h
//  EasyJournal
//
//  Created by James Valaitis on 04/09/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#ifndef EasyJournal_GlobalDefines_h
#define EasyJournal_GlobalDefines_h

#import "Singleton.h"
#import "UIColor+EasyJournal.h"
#import "UIFont+EasyJournalFont.h"
#import "UIImageView+JVAdditions.h"
#import "UIView+JVAdditions.h"
#import "UIViewController+EasyJournal.h"

/**	A notification sent once a transition animation has completed.	*/
static NSString *const EasyJournalTransitionAnimationCompletedNotification			= @"EasyJournalTransitionAnimationCompleted";
/**	A key under which the activity relevant to the animation transition which completed can be retrieved. 	*/
static NSString *const EasyJournalTransitionAnimationCompletedUserInfoActivityKey	= @"EasyJournalTransitionAnimationCompletedUserInfoActivity";

#endif	//	EasyJournal_GlobalDefines_h