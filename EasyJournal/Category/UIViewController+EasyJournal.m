//
//  UIViewController+EasyJournal.m
//  EasyJournal
//
//  Created by James Valaitis on 03/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "UIViewController+EasyJournal.h"

#pragma mark - UIViewController with Easy Journal Additions Implementation

@implementation UIViewController (EasyJournal)

#pragma mark - Adoption

/**
 *	Handles the full adoption of a child view controller.
 *
 *	@param	childViewController			The UIViewController to adopt.
 */
- (void)ej_adoptViewController:(UIViewController *)childViewController
{
	[self.view addSubview:childViewController.view];
	[self addChildViewController:childViewController];
	[childViewController didMoveToParentViewController:self];
}

/**
 *	Handles the full adoption of a child view controller with auto layout in mind.
 *
 *	@param	childViewController			The UIViewController to adopt.
 */
- (void)ej_adoptViewControllerWithAutoLayout:(UIViewController *)childViewController
{
	[self.view addSubviewForAutoLayout:childViewController.view];
	[self addChildViewController:childViewController];
	[childViewController didMoveToParentViewController:self];
}

@end