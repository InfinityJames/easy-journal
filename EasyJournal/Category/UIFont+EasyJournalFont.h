//
//  UIFont+EasyJournalFont.h
//  Credence
//
//  Created by James Valaitis on 08/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - UIFont with Easy Journal Font Public Interface

@interface UIFont (EasyJournalFont) {}

#pragma mark - Public Methods

/**
 *	Returns an instance of the font associated with the text style, scaled appropriately for the user's selected content size category.
 *
 *	@param	textStyle					The text style for for which to return a font.
 *
 *	@return	The font associated with the specified text style.
 */
+ (UIFont *)easyJournalPreferredFontWithStyle:(NSString *)textStyle;

@end