//
//  ActivityNavigationController+Utilities.m
//  EasyJournal
//
//  Created by James Valaitis on 29/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationController+Utilities.h"
#import "ActivityNavigationDataSource.h"

@implementation ActivityNavigationController (Utilities)

#pragma mark - Utility Methods

/**
 *	Returns the frame for the next activity cell relative to a given view.
 *
 *	@param	view						A view whose co-ordinates we use to calculate the frame.
 *
 *	@return	A CGRect describing the frame for a newly added UICollectionViewCell.
 */
- (CGRect)frameForNextActivityRelativeToView:(UIView *)view
{
	return [self.activityNavigationDataSource frameForNextActivityRelativeToView:view];
}

@end