//
//  UIImageView+JVAdditions.m
//  EasyJournal
//
//  Created by James Valaitis on 18/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "UIImageView+JVAdditions.h"

#pragma mark - UIImage with James Valaitis Additions Implementation

@implementation UIImageView (JVAdditions)

/**
 *	A convenient way to get a copy of this image view.
 *
 *	@return	A newly initialised UIImageView with properties represantative to this UIImageView.
 */
- (UIImageView *)jv_imageViewCopy
{
	UIImageView *imageViewCopy			= [[UIImageView alloc] initWithImage:self.image highlightedImage:self.highlightedImage];
	imageViewCopy.frame					= self.frame;
	imageViewCopy.contentMode			= self.contentMode;
	return imageViewCopy;
}

@end