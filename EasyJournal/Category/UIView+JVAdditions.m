//
//  UIView+AutolayoutHelper.m
//  BlueLibrary
//
//  Created by James Valaitis on 17/09/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "UIView+JVAdditions.h"

#pragma mark - UIView with Autolayout Helper Implementation

@implementation UIView (JVAdditions)

#pragma mark - Auto Layout Methods

/**
 *	Adds a view to the end of the receiver’s list of subviews to be positioned using autolayout.
 *
 *	@param	subview						The view to be added. After being added, this view appears on top of any other subviews.
 */
- (void)addSubviewForAutoLayout:(UIView *)subview
{
	subview.translatesAutoresizingMaskIntoConstraints	= NO;
	[self addSubview:subview];
}

#pragma mark - Subview Management

/**
 *	Adds a set of unique subviews to this view.
 *
 *	@param	subviews					A set of subviews to add to this view.
 */
- (void)jv_addSubviews:(NSSet *)subviews
{
	for (UIView *subview in subviews)
		[self addSubview:subview];
}

@end
