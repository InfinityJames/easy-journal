//
//  ActivityNavigationController+Utilities.h
//  EasyJournal
//
//  Created by James Valaitis on 29/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationController.h"

@interface ActivityNavigationController (Utilities)

/**
 *	Returns the frame for the next activity cell relative to a given view.
 *
 *	@param	view						A view whose co-ordinates we use to calculate the frame.
 *
 *	@return	A CGRect describing the frame for a newly added UICollectionViewCell.
 */
- (CGRect)frameForNextActivityRelativeToView:(UIView *)view;

@end