//
//  UIViewController+EasyJournal.h
//  EasyJournal
//
//  Created by James Valaitis on 03/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - UIViewController with Easy Journal Additions Public Interface

@interface UIViewController (EasyJournal) {}

#pragma mark - Public Methods

/**
 *	Handles the full adoption of a child view controller.
 *
 *	@param	childViewController			The UIViewController to adopt.
 */
- (void)ej_adoptViewController:(UIViewController *)childViewController;
/**
 *	Handles the full adoption of a child view controller with auto layout in mind.
 *
 *	@param	childViewController			The UIViewController to adopt.
 */
- (void)ej_adoptViewControllerWithAutoLayout:(UIViewController *)childViewController;

@end