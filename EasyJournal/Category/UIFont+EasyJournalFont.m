//
//  UIFont+EasyJournalFont.m
//  Credence
//
//  Created by James Valaitis on 08/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "UIFont+EasyJournalFont.h"

#pragma mark - UIFont with Easy Journal Font Implementation

@implementation UIFont (EasyJournalFont)

/**
 *	Returns the name of the font associated with the text style.
 *
 *	@param	textStyle					The text style for for which to return a font.
 *
 *	@return	The font name associated with the specified text style.
 */
+ (NSString *)easyJournalFontNameForStyle:(NSString *)textStyle
						   andContentSize:(NSString *)contentSize
{
	static NSDictionary *fontNames;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken,
	^{
		NSDictionary *extraSmall		= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *small				= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *medium			= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *large				= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *extraLarge		= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *extraExtraLarge	= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
											UIFontTextStyleCaption1		: @"AvenirNext-Medium",
											UIFontTextStyleCaption2		: @"AvenirNext-Medium",
											UIFontTextStyleFootnote		: @"AvenirNext-Regular",
											UIFontTextStyleHeadline		: @"AvenirNext-Bold",
											UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		NSDictionary *extraExtraExtraLarge	= @{UIFontTextStyleBody			: @"AvenirNext-Medium",
												UIFontTextStyleCaption1		: @"AvenirNext-Medium",
												UIFontTextStyleCaption2		: @"AvenirNext-Medium",
												UIFontTextStyleFootnote		: @"AvenirNext-Regular",
												UIFontTextStyleHeadline		: @"AvenirNext-Bold",
												UIFontTextStyleSubheadline	: @"AvenirNext-DemiBold"};
		
		fontNames						= @{UIContentSizeCategoryExtraSmall				: extraSmall,
											UIContentSizeCategorySmall					: small,
											UIContentSizeCategoryMedium					: medium,
											UIContentSizeCategoryLarge					: large,
											UIContentSizeCategoryExtraLarge				: extraLarge,
											UIContentSizeCategoryExtraExtraLarge		: extraExtraLarge,
											UIContentSizeCategoryExtraExtraExtraLarge	: extraExtraExtraLarge};
	});
	
	return fontNames[contentSize][textStyle];
}

/**
 *	Returns an instance of the font associated with the text style, scaled appropriately for the user's selected content size category.
 *
 *	@param	textStyle					The text style for for which to return a font.
 *
 *	@return	The font associated with the specified text style.
 */
+ (UIFont *)easyJournalPreferredFontWithStyle:(NSString *)textStyle
{
	//	get the fundamental font for the text style
	NSString *preferredContentSize		= [UIApplication sharedApplication].preferredContentSizeCategory;
	CGFloat fontSize					= [[self easyJournalSizeForStyle:textStyle andContentSize:preferredContentSize] floatValue];
	NSString *fontName					= [self easyJournalFontNameForStyle:textStyle andContentSize:preferredContentSize];
	
	//	adjust the font bassed on the settings
	
	
	return [self fontWithName:fontName size:fontSize];
}

/**
 *	Returns the size of the font associated with the text style.
 *
 *	@param	textStyle					The text style for for which to return a font.
 *
 *	@return	The font size associated with the specified text style.
 */
+ (NSNumber *)easyJournalSizeForStyle:(NSString *)textStyle
					   andContentSize:(NSString *)contentSize
{
	static NSDictionary *fontSizes;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken,
	^{
		NSDictionary *extraSmall		= @{UIFontTextStyleBody			: @(14.0f),
											UIFontTextStyleCaption1		: @(11.0f),
											UIFontTextStyleCaption2		: @(11.0f),
											UIFontTextStyleFootnote		: @(12.0f),
											UIFontTextStyleHeadline		: @(14.0f),
											UIFontTextStyleSubheadline	: @(12.0f)};
		NSDictionary *small				= @{UIFontTextStyleBody			: @(15.0f),
											UIFontTextStyleCaption1		: @(11.0f),
											UIFontTextStyleCaption2		: @(11.0f),
											UIFontTextStyleFootnote		: @(12.0f),
											UIFontTextStyleHeadline		: @(15.0f),
											UIFontTextStyleSubheadline	: @(13.0f)};
		NSDictionary *medium			= @{UIFontTextStyleBody			: @(16.0f),
											UIFontTextStyleCaption1		: @(11.0f),
											UIFontTextStyleCaption2		: @(11.0f),
											UIFontTextStyleFootnote		: @(12.0f),
											UIFontTextStyleHeadline		: @(16.0f),
											UIFontTextStyleSubheadline	: @(14.0f)};
		NSDictionary *large				= @{UIFontTextStyleBody			: @(17.0f),
											UIFontTextStyleCaption1		: @(12.0f),
											UIFontTextStyleCaption2		: @(11.0f),
											UIFontTextStyleFootnote		: @(13.0f),
											UIFontTextStyleHeadline		: @(17.0f),
											UIFontTextStyleSubheadline	: @(15.0f)};
		NSDictionary *extraLarge		= @{UIFontTextStyleBody			: @(18.0f),
											UIFontTextStyleCaption1		: @(13.0f),
											UIFontTextStyleCaption2		: @(12.0f),
											UIFontTextStyleFootnote		: @(14.0f),
											UIFontTextStyleHeadline		: @(18.0f),
											UIFontTextStyleSubheadline	: @(16.0f)};
		NSDictionary *extraExtraLarge	= @{UIFontTextStyleBody			: @(19.0f),
											UIFontTextStyleCaption1		: @(14.0f),
											UIFontTextStyleCaption2		: @(13.0f),
											UIFontTextStyleFootnote		: @(15.0f),
											UIFontTextStyleHeadline		: @(19.0f),
											UIFontTextStyleSubheadline	: @(17.0f)};
		NSDictionary *extraExtraExtraLarge	= @{UIFontTextStyleBody			: @(20.0f),
												UIFontTextStyleCaption1		: @(15.0f),
												UIFontTextStyleCaption2		: @(14.0f),
												UIFontTextStyleFootnote		: @(16.0f),
												UIFontTextStyleHeadline		: @(20.0f),
												UIFontTextStyleSubheadline	: @(18.0f)};
		
		fontSizes						= @{UIContentSizeCategoryExtraSmall				: extraSmall,
											UIContentSizeCategorySmall					: small,
											UIContentSizeCategoryMedium					: medium,
											UIContentSizeCategoryLarge					: large,
											UIContentSizeCategoryExtraLarge				: extraLarge,
											UIContentSizeCategoryExtraExtraLarge		: extraExtraLarge,
											UIContentSizeCategoryExtraExtraExtraLarge	: extraExtraExtraLarge};
	});
	
	return fontSizes[contentSize][textStyle];
}

@end