//
//  UIImageView+JVAdditions.h
//  EasyJournal
//
//  Created by James Valaitis on 18/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#pragma mark - UIImage with James Valaitis Additions Public Interface

@interface UIImageView (JVAdditions)

/**
 *	A convenient way to get a copy of this image view.
 *
 *	@return	A newly initialised UIImageView with properties represantative to this UIImageView.
 */
- (UIImageView *)jv_imageViewCopy;

@end