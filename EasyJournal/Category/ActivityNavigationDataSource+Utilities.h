//
//  ActivityNavigationDataSource+Utilities.h
//  EasyJournal
//
//  Created by James Valaitis on 29/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationDataSource.h"

@interface ActivityNavigationDataSource (Utilities)

#pragma mark - Public Methods

/**
 *	Convenient way to know whether the given index path is last or not.
 *
 *	@param	indexPath					The index path to determine whether it's at the end or not.
 *
 *	@return	YES if the given index path is the last in the collection view, No otherwise.
 */
- (BOOL)indexPathIsLast:(NSIndexPath *)indexPath;
/**
 *	Returns the frame for the next activity cell relative to a given view.
 *
 *	@param	view						A view whose co-ordinates we use to calculate the frame.
 *
 *	@return	A CGRect describing the frame for a newly added UICollectionViewCell.
 */
- (CGRect)frameForNextActivityRelativeToView:(UIView *)view;

@end
