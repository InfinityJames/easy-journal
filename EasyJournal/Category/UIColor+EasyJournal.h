//
//  UIColor+EasyJournal.h
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - UIColor with Easy Journal Colours Public Interface

@interface UIColor (EasyJournal) {}

#pragma mark - Public Methods

/**
 *	A shade of blue to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalBlue;

/**
 *	A shade of blue with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalBlueWithAlpha:(CGFloat)alpha;

/**
 *	A shade of purple to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalPurple;

/**
 *	A shade of purple with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalPurpleWithAlpha:(CGFloat)alpha;

/**
 *	A shade of red to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalRed;

/**
 *	A shade of red with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalRedWithAlpha:(CGFloat)alpha;

/**
 *	A shade of yellow to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalYellow;

/**
 *	A shade of yellow with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalYellowWithAlpha:(CGFloat)alpha;

@end