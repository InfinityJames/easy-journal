//
//  ActivityNavigationDataSource+Utilities.m
//  EasyJournal
//
//  Created by James Valaitis on 29/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationDataSource+Utilities.h"

@implementation ActivityNavigationDataSource (Utilities)

#pragma mark - Utility Methods

/**
 *	Returns the frame for the next activity cell relative to a given view.
 *
 *	@param	view						A view whose co-ordinates we use to calculate the frame.
 *
 *	@return	A CGRect describing the frame for a newly added UICollectionViewCell.
 */
- (CGRect)frameForNextActivityRelativeToView:(UIView *)view
{
	CGRect destinationFrame;
	CGFloat contentInset				= self.activityItemsView.contentInset.left;
	NSUInteger itemCount				= self.activityItemsView.indexPathsForVisibleItems.count;
	CGSize itemSize						= ((UICollectionViewFlowLayout *)self.activityItemsView.collectionViewLayout).itemSize;
	
	if (itemCount > 0)
	{
		NSArray *indexPathsForVisibleItems	= [self.activityItemsView.indexPathsForVisibleItems sortedArrayUsingComparator:^NSComparisonResult(NSIndexPath *indexPathA, NSIndexPath *indexPathB)
											   {
												   if (indexPathA.item > indexPathB.item)
													   return NSOrderedDescending;
												   else if (indexPathA.item < indexPathB.item)
													   return NSOrderedAscending;
												   else
													   return NSOrderedSame;
											   }];
		
		UICollectionViewCell *cell		= [self.activityItemsView cellForItemAtIndexPath:indexPathsForVisibleItems[itemCount - 1]];
		CGFloat itemSpacing				= ((UICollectionViewFlowLayout *)self.activityItemsView.collectionViewLayout).minimumInteritemSpacing;
		destinationFrame				= cell.frame;
		destinationFrame.origin			= CGPointMake(CGRectGetMinX(destinationFrame) + contentInset + itemSpacing + itemSize.width - 6.0f,
													  CGRectGetMinY(destinationFrame) + contentInset);
	}
	else
	{
		destinationFrame.origin			= CGPointMake(contentInset, contentInset);
		destinationFrame.size			= itemSize;
	}
	
	CGRect convertedFrame				= [self.activityItemsView convertRect:destinationFrame toView:view];
	convertedFrame.origin				= CGPointMake(CGRectGetMinX(convertedFrame) - contentInset,
													  CGRectGetMinY(convertedFrame) - contentInset);
	
	return convertedFrame;
}

/**
 *	Convenient way to know whether the given index path is last or not.
 *
 *	@param	indexPath					The index path to determine whether it's at the end or not.
 *
 *	@return	YES if the given index path is the last in the collection view, No otherwise.
 */
- (BOOL)indexPathIsLast:(NSIndexPath *)indexPath
{
	BOOL indexPathIsLast				= indexPath.item == self.activities.count;
	return indexPathIsLast;
}

@end
