//
//  UIColor+EasyJournal.m
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "UIColor+EasyJournal.h"

#pragma mark - UIColor with Easy Journal Colours Implementation

@implementation UIColor (EasyJournal)

#pragma mark - Basic Colours

/**
 *	A shade of blue to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalBlue
{
	return [[UIColor alloc] initWithRed:074.0f / 255.0f green:209.0f / 255.0f blue:190.0f / 255.0f alpha:1.0f];
}

/**
 *	A shade of blue with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalBlueWithAlpha:(CGFloat)alpha
{
	return [[UIColor alloc] initWithRed:074.0f / 255.0f green:209.0f / 255.0f blue:190.0f / 255.0f alpha:alpha];
}

/**
 *	A shade of purple to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalPurple
{
	return [[UIColor alloc] initWithRed:175.0f / 255.0f green:124.0f / 255.0f blue:208.0f / 255.0f alpha:1.0f];
}

/**
 *	A shade of purple with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalPurpleWithAlpha:(CGFloat)alpha
{
	return [[UIColor alloc] initWithRed:175.0f / 255.0f green:124.0f / 255.0f blue:208.0f / 255.0f alpha:alpha];
}

/**
 *	A shade of red to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalRed
{
	return [[UIColor alloc] initWithRed:230.0f / 255.0f green:064.0f / 255.0f blue:000.0f / 255.0f alpha:1.0f];
}

/**
 *	A shade of red with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalRedWithAlpha:(CGFloat)alpha
{
	return [[UIColor alloc] initWithRed:230.0f / 255.0f green:064.0f / 255.0f blue:000.0f / 255.0f alpha:alpha];
}

/**
 *	A shade of yellow to be used through this app.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalYellow
{
	return [[UIColor alloc] initWithRed:255.0f / 255.0f green:232.0f / 255.0f blue:090.0f / 255.0f alpha:1.0f];
}

/**
 *	A shade of yellow with a given alpha to be used through this app.
 *
 *	@param	alpha						The alpha for the UIColour.
 *
 *	@return	An initialised UIColor.
 */
+ (UIColor *)easyJournalYellowWithAlpha:(CGFloat)alpha
{
	return [[UIColor alloc] initWithRed:255.0f / 255.0f green:232.0f / 255.0f blue:090.0f / 255.0f alpha:alpha];
}

@end