//
//  GeneralActivityLoader.m
//  EasyJournal
//
//  Created by James Valaitis on 31/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "GeneralActivityLoader.h"

#pragma mark - Constants & Static Variables

/**	The key in a dictionary for the activity type of the general activity.	*/
static NSString *const GeneralActivityActivityTypeKey	= @"type";
/**	The key in a dictionary for the name of the image for the general activity.	*/
static NSString *const GeneralActivityImageNameKey		= @"image";
/**	The key in a dictionary for the title of the general activity.	*/
static NSString *const GeneralActivityTitleKey			= @"title";

#pragma mark - General Activity Loader Implementation

@implementation GeneralActivityLoader {}

#pragma mark - Loading

/**
 *	Loads the general activities from a source file and returns them.
 *
 *	@return	An NSArray of GeneralActivity objects.
 */
- (NSArray *)generalActivities
{
	static NSArray *generalActivities;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken,
	^{
		NSString *filePath						= [[NSBundle mainBundle] pathForResource:@"general" ofType:@"plist"];
		NSArray *generalActivityDictionaries	= [[NSArray alloc] initWithContentsOfFile:filePath];
		
		NSMutableArray *mutableActivities		= [[NSMutableArray alloc] initWithCapacity:generalActivityDictionaries.count];
		
		for (NSDictionary *generalActivityDictionary in generalActivityDictionaries)
		{
			NSString *title						= generalActivityDictionary[GeneralActivityTitleKey];
			UIImage *image						= [UIImage imageNamed:generalActivityDictionary[GeneralActivityImageNameKey]];
			NSString *activityType				= generalActivityDictionary[GeneralActivityActivityTypeKey];
			GeneralActivity *generalActivity	= [[GeneralActivity alloc] initWithTitle:title image:image activityType:activityType];
			[mutableActivities addObject:generalActivity];
		}
		
		NSSortDescriptor *titleSorter			= [[NSSortDescriptor alloc] initWithKey:@"title"
																	 ascending:YES selector:@selector(localizedStandardCompare:)];
		
		generalActivities						= [mutableActivities sortedArrayUsingDescriptors:@[titleSorter]];
	});
	
	return generalActivities;
}

@end