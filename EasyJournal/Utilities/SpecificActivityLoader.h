//
//  SpecificActivityLoader.h
//  EasyJournal
//
//  Created by James Valaitis on 09/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "SpecificActivity.h"

#pragma mark - Specific Activity Loader Public Interface

@interface SpecificActivityLoader : NSObject

#pragma mark - Public Methods

/**
 *	Loads the specific activities from a source file for a specific type and returns them.
 *
 *	@return	An NSArray of SpecificActivity objects.
 */
- (NSArray *)specificActivitiesOfType:(NSString *)activityType;

@end