//
//  SpecificActivityLoader.m
//  EasyJournal
//
//  Created by James Valaitis on 09/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "SpecificActivityLoader.h"

#pragma mark - Constants & Static Variables

/**	The key in a dictionary for the name of the image for the general activity.	*/
static NSString *const SpecificActivityImageNameKey		= @"image";
/**	The key in a dictionary for the title of the general activity.	*/
static NSString *const SpecificActivityTitleKey			= @"title";

#pragma mark - Specific Activity Loader Private Class Extension

@interface SpecificActivityLoader ()

#pragma mark - Private Properties

/**	The type of the specific activities this class is in charge of loading.	*/
@property (nonatomic, strong)	NSString		*activityType;

@end

#pragma mark - Specific Activity Loader Implementation

@implementation SpecificActivityLoader

#pragma mark - Loading

/**
 *	Loads the specific activities from a source file for a specific type and returns them.
 *
 *	@return	An NSArray of SpecificActivity objects.
 */
- (NSArray *)specificActivitiesOfType:(NSString *)activityType
{
	NSArray *specificActivities;
	
	NSString *filePath						= [[NSBundle mainBundle] pathForResource:activityType ofType:@"plist"];
	NSArray *specificActivityDictionaries	= [[NSArray alloc] initWithContentsOfFile:filePath];
	
	NSMutableArray *mutableActivities		= [[NSMutableArray alloc] initWithCapacity:specificActivityDictionaries.count];
	
	for (NSDictionary *generalActivityDictionary in specificActivityDictionaries)
	{
		NSString *title						= generalActivityDictionary[SpecificActivityTitleKey];
		UIImage *image						= [UIImage imageNamed:generalActivityDictionary[SpecificActivityImageNameKey]];
		SpecificActivity *specificActivity	= [[SpecificActivity alloc] initWithTitle:title image:image];
		[mutableActivities addObject:specificActivity];
	}
				  
	NSSortDescriptor *titleSorter			= [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedStandardCompare:)];
	
	specificActivities						= [mutableActivities sortedArrayUsingDescriptors:@[titleSorter]];
	
	return specificActivities;
}

@end