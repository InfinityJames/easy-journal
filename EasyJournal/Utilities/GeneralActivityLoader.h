//
//  GeneralActivityLoader.h
//  EasyJournal
//
//  Created by James Valaitis on 31/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "GeneralActivity.h"

#pragma mark - General Activity Loader Public Interface

@interface GeneralActivityLoader : Singleton {}

#pragma mark - Public Properties

/**	An array of all GeneralActivity objects.	*/
@property (nonatomic, strong, readonly)		NSArray		*generalActivities;

@end