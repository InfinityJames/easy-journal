//
//  GeneralActivity.h
//  EasyJournal
//
//  Created by James Valaitis on 09/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityProtocol.h"

#pragma mark - General Activity Public Interface

@interface GeneralActivity : NSObject <ActivityProtocol> {}

#pragma mark - Public Properties: Activity Protocol

/**	An image to be used to symbolise this activity at 128px x 128px.	*/
@property (nonatomic, strong, readonly)		UIImage		*activityImage;
/**	The title of the activity.	*/
@property (nonatomic, strong, readonly)		NSString	*title;

#pragma mark - Public Properties: State

/**	Used to define the type of activities this represents.	*/
@property (nonatomic, strong, readonly)		NSString	*activityType;

#pragma mark - Public Methods

/**
 *	Initialises a new GeneralActity, representing a very general type of activity,
 *
 *	@param	title						The title of the activity.
 *	@param	activityImage				An image symbolising the activity.
 *	@param	activityType				The type of activity this relates to.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title
						image:(UIImage *)activityImage
				 activityType:(NSString *)activityType;

@end