//
//  ActivityNavigationDataSource.h
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

@class ActivityNavigationRootItem;
@protocol ActivityProtocol;

#pragma mark - Constants & Static Variables

/**	The unique identifier for reusing the cells.	*/
extern NSString *const ActivityNavigationCellIdentifier;

#pragma mark - Activity Navigation Item Public Interface

@interface ActivityNavigationDataSource : NSObject {}

#pragma mark - Public Properties

/**	The array of activities shown in the activity item's view.	*/
@property (nonatomic, strong, readonly)		NSMutableArray		*activities;
/**	The ActivityItemsView which this object is in charge of.	*/
@property (nonatomic, weak, readonly)		UICollectionView	*activityItemsView;

#pragma mark - Public Methods

/**
 *	Initialises an instance of this ActivityNavigationDataSource associated with the given activityItemsView.
 *
 *	@param	activityItemsView			The activities view for which this object should manage.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityItemsView:(UICollectionView *)activityItemsView;

/**
 *	Initialises an instance of this ActivityNavigationDataSource associated with the given activityItemsView.
 *
 *	@param	activityItemsView			The activities view for which this object should manage.
 *	@param	rootItem					The root item for the activities view being managed.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityItemsView:(UICollectionView *)activityItemsView
					andRootNavigationItem:(ActivityNavigationRootItem *)rootItem;

/**
 *	Adds an activity as a cell to the end of presented cells.
 *
 *	@param	activity					The activity item to be displayed a cell at the end of the view.
 */
- (void)pushActivityAsCell:(id <ActivityProtocol>)activity;

/**
 *	Removes all activities displayed in the activity items view.
 */
- (void)popAllActivities;

/**
 *	Removes all displayed activities ahead of the activity at the given index.
 *
 *	@param	activityIndex				The index of the activity to pop back to.
 */
- (void)popBackToActivityAtIndex:(NSUInteger)activityIndex;

@end

#import "ActivityNavigationDataSource+Utilities.h"