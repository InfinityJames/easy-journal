//
//  ActivityNavigationDelegate.m
//  EasyJournal
//
//  Created by James Valaitis on 03/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityNavigationDelegate.h"
#import "EasyJournalTransitionAnimationController.h"

#pragma mark - Activity Navigation Delegate Private Class Extension

@interface ActivityNavigationDelegate () {}

#pragma mark - Private Properties

/**	Handles the animating of the transition of a UIViewController in the UINavigationController.	*/
@property (nonatomic, strong)		EasyJournalTransitionAnimationController		*transitionController;

@end

#pragma mark - Activity Navigation Delegate Implementation

@implementation ActivityNavigationDelegate {}

#pragma mark - Convenience & Helper Methods

/**
 *	Allows convenient access to the transition controller in a state of a popping transition.
 *
 *	@return	The EasyJournalTransitionAnimationController in charge a popping transition animation.
 */
- (EasyJournalTransitionAnimationController *)poppingTransitionController
{
	self.transitionController.reverse	= YES;
	//	reverses the transition controller and returns it
	return self.transitionController;
}

/**
 *	Allows convenient access to the transition controller in a state of a pushing transition.
 *
 *	@return	The EasyJournalTransitionAnimationController in charge a pushing transition animation.
 */
- (EasyJournalTransitionAnimationController *)pushingTransitionController
{
	self.transitionController.reverse	= NO;
	//	makes the transition controller work forward and returns it
	return self.transitionController;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	Handles the animating of the transition of a UIViewController in the UINavigationController.
 *
 *	@return	Handles the animating of the transition of a UIViewController in the UINavigationController.
 */
- (EasyJournalTransitionAnimationController *)transitionController
{
	if (!_transitionController)
		_transitionController			= [[EasyJournalTransitionAnimationController alloc] init];
	
	return _transitionController;
}

#pragma mark - UINavigationControllerDelegate Methods

/**
 *	Called to allow the delegate to return a noninteractive animator object for use during view controller transitions.
 *
 *	@param	navigationController		The navigation controller whose navigation stack is changing.
 *	@param	operation					The type of transition operation that is occurring.
 *	@param	fromViewController			The currently visible view controller.
 *	@param	toViewController			The view controller that should be visible at the end of the transition.
 *
 *	@return	The animator object responsible for managing the transition animations, or nil to use the standard navigation controller transitions.
 */
- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
								   animationControllerForOperation:(UINavigationControllerOperation)operation
												fromViewController:(UIViewController *)fromViewController
												  toViewController:(UIViewController *)toViewController
{
	return operation == UINavigationControllerOperationPush ? self.pushingTransitionController : self.poppingTransitionController;
}

/**
 *	Called just after the navigation controller displays a view controller’s view and navigation item properties.
 *
 *	@param	navigationController		The navigation controller that is showing the view and properties of a view controller.
 *	@param	viewController				The view controller whose view and navigation item properties are being shown.
 *	@param	animated					YES to animate the transition; otherwise, NO.
 */
- (void)navigationController:(UINavigationController *)navigationController
	   didShowViewController:(UIViewController *)viewController
					animated:(BOOL)animated
{
	if (self.completionHandler)
	{
		self.completionHandler();
		self.completionHandler			= nil;
	}
}

/**
 *	Called to allow the delegate to return an interactive animator object for use during view controller transitions.
 *
 *	@param	navigationController		The navigation controller whose navigation stack is changing.
 *	@param	animationController			The noninteractive animator object provided by the delegate.
 *
 *	@return	The animator object responsible for managing the transition animations, or nil to use the standard navigation controller transitions.
 */
- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
						  interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController
{
	return nil;
}

@end