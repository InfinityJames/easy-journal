//
//  SpecificActivity.h
//  EasyJournal
//
//  Created by James Valaitis on 09/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityProtocol.h"

#pragma mark - Specific Activity Public Interface

@interface SpecificActivity : NSObject <ActivityProtocol>

#pragma mark - Public Properties: Activity Protocol

/**	An image to be used to symbolise this activity at 128px x 128px.	*/
@property (nonatomic, strong, readonly)		UIImage		*activityImage;
/**	The title of the activity.	*/
@property (nonatomic, strong, readonly)		NSString	*title;

#pragma mark - Public Methods

/**
 *	Initialises a new SpecificActivity, representing one of the specific types of activities of a more general activity.
 *
 *	@param	title						The title of the activity.
 *	@param	activityImage				An image symbolising the activity.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title
						image:(UIImage *)activityImage;

@end