//
//  GeneralActivity.m
//  EasyJournal
//
//  Created by James Valaitis on 09/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "GeneralActivity.h"

#pragma mark - General Activity Implementation

@implementation GeneralActivity {}

#pragma mark - Initialisation

/**
 *	Initialises a new GeneralActity, representing a very general type of activity,
 *
 *	@param	title						The title of the activity.
 *	@param	activityImage				An image symbolising the activity.
 *	@param	activityType				The type of activity this relates to.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title
						image:(UIImage *)activityImage
				 activityType:(NSString *)activityType
{
	if (self = [super init])
	{
		//	activity protocol properties
		_activityImage					= activityImage;
		_title							= title;
		
		_activityType					= activityType;
	}
	
	return self;
}

@end