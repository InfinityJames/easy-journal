//
//  SpecificActivity.m
//  EasyJournal
//
//  Created by James Valaitis on 09/03/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "SpecificActivity.h"

#pragma mark - Specific Activity Implementation

@implementation SpecificActivity

#pragma mark - Initialisation

/**
 *	Initialises a new SpecificActivity, representing one of the specific types of activities of a more general activity.
 *
 *	@param	title						The title of the activity.
 *	@param	activityImage				An image symbolising the activity.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title
						image:(UIImage *)activityImage
{
	if (self = [super init])
	{
		//	activity protocol properties
		_activityImage					= activityImage;
		_title							= title;
	}
	
	return self;
}

@end