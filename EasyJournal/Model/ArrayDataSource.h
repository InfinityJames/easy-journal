//
//  ArrayDataSource.h
//  MakeAMealOfIt
//
//  Created by James Valaitis on 26/08/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

@class ArrayDataSource;

#pragma mark - Type Definitions

/**	A block to be used when configuring a cell with a given item.	*/
typedef void (^CellConfigureBlock)(id cell, id item, NSIndexPath *indexPath);

#pragma mark - Array Data Source Delegate Declaration

@protocol ArrayDataSourceDelegate <NSObject>

@required

#pragma mark - Required Methods

/**
 *	Used to configure a cell with a given item.
 *
 *	@param	arrayDataSource				The array data source calling for this configuration.
 *	@param	cell						The table view / collection view cell requiring configuration.
 *	@param	item						The item associated with the given cell.
  *	@param	indexPath					The index path of the given cell.
 */
- (void)arrayDataSource:(ArrayDataSource *)arrayDataSource
		  configureCell:(id)cell
			   withItem:(id)item
			atIndexPath:(NSIndexPath *)indexPath;

@end

#pragma mark - Array Data Source Public Interface

@interface ArrayDataSource : NSObject <UICollectionViewDataSource, UITableViewDataSource> {}

#pragma mark - Public Properties

/**	The delegate used to do work we can't do with cells.	*/
@property (nonatomic, weak)		id <ArrayDataSourceDelegate>		delegate;

#pragma mark - Public Methods

/**
 *	Initialises an array data source to be used for UITableViews / UICollectionViews.
 *
 *	@param	items						The array of items to be used as the data source.
 *	@param	cellIdentifier				The unique identifier to be used for reusing cells.
 *	@param	configureCellBlock			A block to be used when configuring a cell with a given item.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithItems:(NSArray *)items
			   cellIdentifier:(NSString *)cellIdentifier
		andConfigureCellBlock:(CellConfigureBlock)configureCellBlock;
/**
 *	Initialises an array data source to be used for UITableViews / UICollectionViews.
 *
 *	@param	items						The array of items to be used as the data source.
 *	@param	cellIdentifier				The unique identifier to be used for reusing cells.
 *	@param	delegate					The delegate used to do work with cells.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithItems:(NSArray *)items
			   cellIdentifier:(NSString *)cellIdentifier
				  andDelegate:(id <ArrayDataSourceDelegate>)delegate;
/**
 *	A convenient way to get a UICollectionViewCell's item given it's index path.
 *
 *	@param	indexPath					The index path of the cell requiring the item.
 *
 *	@return	The appropriate item for a given index path.
 */
- (id)itemForItemAtIndexPath:(NSIndexPath *)indexPath;
/**
 *	A convenient way to get a UITableViewCell's item given it's index path.
 *
 *	@param	indexPath					The index path of the cell requiring the item.
 *
 *	@return	The appropriate item for a given index path.
 */
- (id)itemForRowAtIndexPath:(NSIndexPath *)indexPath;
/**
 *	Updates this array data source with recently edited items.
 *
 *	@param	items						The array of items to be used as the data source.
 */
- (void)updateWithItems:(NSArray *)items;

@end