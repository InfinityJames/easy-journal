//
//  ActivityNavigationDataSource.m
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityProtocol.h"
#import "ActivityNavigationDataSource.h"
#import "ActivityNavigationRootItem.h"
#import "ActivityNavigationViewCell.h"

#pragma mark - Constants & Static Variables

/**	The unique identifier for reusing the cells.	*/
NSString *const ActivityNavigationCellIdentifier		= @"ActivityNavigationDataSource.CellIdentifier";

#pragma mark - Activity Navigation Item Private Class Extension

@interface ActivityNavigationDataSource () <UICollectionViewDataSource> {}

#pragma mark - Private Properties

/**	The array of activities shown in the activity item's view.	*/
@property (nonatomic, strong, readwrite)		NSMutableArray				*activities;
/**	The ActivityItemsView which this object is in charge of.	*/
@property (nonatomic, weak, readwrite)		UICollectionView			*activityItemsView;
/**	The root item for the activities view being managed.	*/
@property (nonatomic, weak)					ActivityNavigationRootItem	*rootNavigationItem;

@end

#pragma mark - Activity Navigation Item Implementation

@implementation ActivityNavigationDataSource {}

#pragma mark - Initialisation

/**
 *	Initialises an instance of this ActivityNavigationDataSource associated with the given activityItemsView.
 *
 *	@param	activityItemsView			The activities view for which this object should manage.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityItemsView:(UICollectionView *)activityItemsView;
{
	if (self = [super init])
	{
		_activityItemsView				= activityItemsView;
		_activityItemsView.dataSource	= self;
		[_activityItemsView registerClass:[ActivityNavigationViewCell class] forCellWithReuseIdentifier:ActivityNavigationCellIdentifier];
	}
	
	return self;
}

/**
 *	Initialises an instance of this ActivityNavigationDataSource associated with the given activityItemsView.
 *
 *	@param	activityItemsView			The activities view for which this object should manage.
 *	@param	rootItem					The root item for the activities view being managed.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityItemsView:(UICollectionView *)activityItemsView
					andRootNavigationItem:(ActivityNavigationRootItem *)rootItem
{
	if (self = [super init])
	{
		_rootNavigationItem				= rootItem;
		
		_activityItemsView				= activityItemsView;
		_activityItemsView.dataSource	= self;
		[_activityItemsView registerClass:[ActivityNavigationViewCell class] forCellWithReuseIdentifier:ActivityNavigationCellIdentifier];
	}
	
	return self;
}

#pragma mark - Insertion & Deletion

/**
 *	Removes all activities displayed in the activity items view.
 */
- (void)popAllActivities
{
	NSUInteger allItemsCount			= self.activities.count;
	[self.activities removeAllObjects];
	
	NSMutableArray *indexPaths			= [[NSMutableArray alloc] init];
	for (NSUInteger index = 1; index <= allItemsCount; index++)
	{
		NSIndexPath *indexPath			= [NSIndexPath indexPathForItem:index inSection:0];
		[indexPaths addObject:indexPath];
	}
	
	[self.activityItemsView deleteItemsAtIndexPaths:indexPaths];
}

/**
 *	Removes all displayed activities ahead of the activity at the given index.
 *
 *	@param	activityIndex				The index of the activity to pop back to.
 */
- (void)popBackToActivityAtIndex:(NSUInteger)activityIndex
{
	NSUInteger activitiesCount			= self.activities.count;
	NSMutableArray *indexPaths			= [[NSMutableArray alloc] init];
	
	for (NSUInteger index = activityIndex + 1; index <= activitiesCount; index++)
	{
		NSIndexPath *indexPath			= [NSIndexPath indexPathForItem:index inSection:0];
		[indexPaths addObject:indexPath];
	}
	
	[self.activities removeObjectsInRange:NSMakeRange(activityIndex, activitiesCount - activityIndex)];
	[self.activityItemsView deleteItemsAtIndexPaths:indexPaths];
}

/**
 *	Adds an activity as a cell to the end of presented cells.
 *
 *	@param	activity					The activity item to be displayed a cell at the end of the view.
 */
- (void)pushActivityAsCell:(id <ActivityProtocol>)activity
{
	[self.activities addObject:activity];
	[self.activityItemsView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.activities.count inSection:0]]];
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	The array of activities shown in the activity item's view.
 *
 *	@return	The array of activities shown in the activity item's view.
 */
- (NSMutableArray *)activities
{
	if (!_activities)
	{
		_activities						= [[NSMutableArray alloc] init];
	}
	
	return _activities;
}

#pragma mark - Property Accessor Methods - Setters

/**
 *	The ActivityItemsView which this object is in charge of.
 *
 *	@param	activityItemsView			The ActivityItemsView which this object is in charge of.
 */
- (void)setActivityItemsView:(UICollectionView *)activityItemsView
{
	if (_activityItemsView == activityItemsView)
		return;
	
	_activityItemsView					= activityItemsView;
	
	if (!_activityItemsView)			return;
	
	_activityItemsView.dataSource		= self;
	[_activityItemsView registerClass:[ActivityNavigationViewCell class] forCellWithReuseIdentifier:ActivityNavigationCellIdentifier];
}

#pragma mark - UICollectionViewDataSource Methods

/**
 *	As the data source we return the cell that corresponds to the specified item in the collection view.
 *
 *	@param	collectionView				Object representing the collection view requesting this information.
 *	@param	indexPath					Index path that specifies the location of the item.
 *
 *	@return	A collection view cell appropriate for the given index path.
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
				  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	ActivityNavigationViewCell *cell	= [collectionView dequeueReusableCellWithReuseIdentifier:ActivityNavigationCellIdentifier
																			 forIndexPath:indexPath];
	
	UIImage *activityImage;
	
	if (indexPath.row == 0)
	{
		ActivityNavigationRootItemOption *rootItemOption	= self.rootNavigationItem.rootOptions[0];
		activityImage					= rootItemOption.optionImage;
	}
	
	else
	{
		NSUInteger index				= indexPath.row - 1;
		activityImage					= ((id<ActivityProtocol>)self.activities[index]).activityImage;
	}
	
	cell.activityImage					= activityImage;
	
	return cell;
}

/**
 *	Asks the data source for the number of items in the specified section.
 *
 *	@param	collectionView				An object representing the collection view requesting this information.
 *	@param	section						An index number identifying a section in collectionView. This index value is 0-based.
 *
 *	@return	The number of rows in section.
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView
	 numberOfItemsInSection:(NSInteger)section
{
	return self.activities.count + 1;
}

/**
 *	Asks the data source for the number of sections in the collection view.
 *
 *	@param	collectionView				An object representing the collection view requesting this information.
 *
 *	@return	The number of sections in collectionView.
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

@end