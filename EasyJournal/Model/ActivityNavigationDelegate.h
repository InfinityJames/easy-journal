//
//  ActivityNavigationDelegate.h
//  EasyJournal
//
//  Created by James Valaitis on 03/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - Activity Navigation Delegate Public Interface

@interface ActivityNavigationDelegate : NSObject <UINavigationControllerDelegate> {}

/**	A block called upon the completion of a presentation of a view controller.	*/
@property (nonatomic, copy)		void(^completionHandler)();

@end