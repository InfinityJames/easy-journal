//
//  EasyJournalAnimatableProtocol.h
//  EasyJournal
//
//  Created by James Valaitis on 05/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

@protocol ActivityProtocol;

#pragma mark - Easy Journal Animatable Protocol Declaration

@protocol EasyJournalAnimatableProtocol <NSObject>

#pragma mark - Required Methods

@required

#pragma mark - From View Controller Methods

/**
 *	Requesting the activity object which relates to a given view.
 *
 *	@param	view						The view for which we need the appropriate activity object.
 *
 *	@return	An NSObject conforming to the ActivityProtocol which relates to the passed UIView.
 */
- (id <ActivityProtocol>)activityForView:(UIView *)view;

/**
 *	Requesting the destination frame for a given view.
 *
 *	@param	view						The view for which we need a destination frame.
 *
 *	@return	A CGRect detailing the desired frame for the given view by the end of the animation.
 */
- (CGRect)destinationFrameForView:(UIView *)view;
/**
 *	An array of views to dismiss during the animation.
 *
 *	@return	An NSArray of UIViews to de dismissed during the animation.
 */
- (NSArray *)viewsToDismiss;
/**
 *	The view to keep through the animation.
 *
 *	@return	A UIView to keep throughout the transition animation.
 */
- (UIView *)viewToKeep;

@end