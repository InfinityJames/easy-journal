//
//  ActivityProtocol.h
//  EasyJournal
//
//  Created by James Valaitis on 09/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - Activity Protocol Declaration

@protocol ActivityProtocol <NSObject>

#pragma mark - Required Properties

@required

/**	An image to be used to symbolise this activity at 128px x 128px.	*/
@property (nonatomic, strong, readonly)		UIImage		*activityImage;
/**	The title of the activity.	*/
@property (nonatomic, strong, readonly)		NSString	*title;

@end