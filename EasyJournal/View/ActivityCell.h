//
//  ActivityCell.h
//  EasyJournal
//
//  Created by James Valaitis on 31/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - Activity Cell Public Interface

@interface ActivityCell : UITableViewCell {}

#pragma mark - Public Properties

/**	An image symbolising the activity being displayed.	*/
@property (nonatomic, strong)			UIImage		*activityImage;
/**	A view displaying the thumbnail of the activity being displayed.	*/
@property (nonatomic, strong, readonly)	UIImageView	*activityThumbnailView;
/**	The title of the activity being displayed.	*/
@property (nonatomic, strong)			NSString	*activityTitle;

#pragma mark - Public Methods

/**
 *	Initialised a new ActivityCell with the activity's title and image.
 *
 *	@param	title						The title of the activity displayed by the cell.
 *	@param	image						An image symbolising the activity displayed by the cell.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image;

@end