//
//  ActivityNavigationViewCell.m
//  EasyJournal
//
//  Created by James Valaitis on 25/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationViewCell.h"

#pragma mark - Activity Navigation View Cell Private Class Extension

@interface ActivityNavigationViewCell () {}

#pragma mark - Private Properties

/**	A view displaying the thumbnail of the activity being displayed.	*/
@property (nonatomic, strong)	UIImageView		*activityImageView;

@end

#pragma mark - Activity Navigation View Cell Implementation

@implementation ActivityNavigationViewCell {}

#pragma mark - Auto Layout Methods

/**
 *	Returns whether the receiver depends on the constraint-based layout system.
 *
 *	@return	YES if the view must be in a window using constraint-based layout to function properly, NO otherwise.
 */
+ (BOOL)requiresConstraintBasedLayout
{
	return YES;
}

/**
 *	Update constraints for the view.
 */
- (void)updateConstraints
{
	[super updateConstraints];
	
	[self removeConstraints:self.constraints];
	[self.contentView removeConstraints:self.contentView.constraints];
	
	NSDictionary *viewsDictionary		= @{@"activityImageView"	: self.activityImageView};
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[activityImageView]|"
																			 options:kNilOptions
																			 metrics:nil
																			   views:viewsDictionary]];
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[activityImageView]|"
																			 options:kNilOptions
																			 metrics:nil
																			   views:viewsDictionary]];
}

#pragma mark - Initialisation

/**
 *	Initialises a collection view configured to represent an activity.
 *
 *	@param	activityImage				An image symbolising the activity displayed by the cell.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityImage:(UIImage *)activityImage
{
	if (self = [super init])
	{
		_activityImage					= activityImage;
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	A view displaying the thumbnail of the activity being displayed.
 *
 *	@return	A view displaying the thumbnail of the activity being displayed.
 */
- (UIImageView *)activityImageView
{
	if (!_activityImageView)
	{
		_activityImageView				= [[UIImageView alloc] initWithImage:self.activityImage];
		_activityImageView.contentMode	= UIViewContentModeScaleAspectFill;
		
		[self.contentView addSubviewForAutoLayout:_activityImageView];
	}
	
	return _activityImageView;
}

#pragma mark - Property Accessor Methods - Setters

/**
 *	An image symbolising the activity displayed by the cell.
 *
 *	@param	activityImage				An image symbolising the activity displayed by the cell.
 */
- (void)setActivityImage:(UIImage *)activityImage
{
	if (_activityImage == activityImage)
		return;
	
	_activityImage						= activityImage;
	
	self.activityImageView.image		= _activityImage;
}

@end