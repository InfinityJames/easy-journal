//
//  ActivityNavigationViewCell.h
//  EasyJournal
//
//  Created by James Valaitis on 25/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#pragma mark - Activity Navigation View Cell Public Interface

@interface ActivityNavigationViewCell : UICollectionViewCell {}

#pragma mark - Public Properties

/**	An image symbolising the activity displayed by the cell.	*/
@property (nonatomic, strong)	UIImage		*activityImage;

#pragma mark - Public Methods

/**
 *	Initialises a collection view configured to represent an activity.
 *
 *	@param	activityImage				An image symbolising the activity displayed by the cell.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityImage:(UIImage *)activityImage;

@end