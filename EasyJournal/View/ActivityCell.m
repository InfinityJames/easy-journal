//
//  ActivityCell.m
//  EasyJournal
//
//  Created by James Valaitis on 31/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityCell.h"

#pragma mark - Activity Cell Private Class Extension

@interface ActivityCell () {}

#pragma mark - Private Properties

/**	The label displaying the activity title.	*/
@property (nonatomic, strong)				UILabel			*activityLabel;
/**	A view displaying the thumbnail of the activity being displayed.	*/
@property (nonatomic, strong, readwrite)	UIImageView		*activityThumbnailView;

@end

#pragma mark - Activity Cell Implementation

@implementation ActivityCell {}

#pragma mark - Auto Layout Methods

/**
 *	Returns whether the receiver depends on the constraint-based layout system.
 *
 *	@return	YES if the view must be in a window using constraint-based layout to function properly, NO otherwise.
 */
+ (BOOL)requiresConstraintBasedLayout
{
	return YES;
}

/**
 *	Update constraints for the view.
 */
- (void)updateConstraints
{
	[super updateConstraints];
	
	[self removeConstraints:self.constraints];
	[self.contentView removeConstraints:self.contentView.constraints];
	
	//NSDictionary *viewsDictionary		= @{@"label"	: self.activityLabel,
	//										@"image"	: self.activityThumbnailView	};
	
	//	vertically position the subviews
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityLabel
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.contentView
																 attribute:NSLayoutAttributeCenterY
																multiplier:1.0f
																  constant:0.0f]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityThumbnailView
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.activityLabel
																 attribute:NSLayoutAttributeCenterY
																multiplier:1.0f
																  constant:0.0f]];
	
	//	horizontally position the subviews
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityLabel
																 attribute:NSLayoutAttributeCenterX
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.contentView
																 attribute:NSLayoutAttributeCenterX
																multiplier:1.0f
																  constant:0.0f]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityThumbnailView
																 attribute:NSLayoutAttributeLeading
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.contentView
																 attribute:NSLayoutAttributeLeading
																multiplier:1.0f
																  constant:32.0f]];
	
	//	size the thumbnail
	[self.activityThumbnailView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityThumbnailView
																		   attribute:NSLayoutAttributeWidth
																		   relatedBy:NSLayoutRelationEqual
																			  toItem:nil
																		   attribute:NSLayoutAttributeNotAnAttribute
																		  multiplier:1.0f
																			constant:32.0f]];
	[self.activityThumbnailView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityThumbnailView
																		   attribute:NSLayoutAttributeHeight
																		   relatedBy:NSLayoutRelationEqual
																			  toItem:self.activityThumbnailView
																		   attribute:NSLayoutAttributeWidth
																		  multiplier:1.0f
																			constant:0.0f]];
}

#pragma mark - Initialisation

/**
 *	Initialised a new ActivityCell with the activity's title and image.
 *
 *	@param	title						The title of the activity displayed by the cell.
 *	@param	image						An image symbolising the activity displayed by the cell.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image
{
	if (self = [super init])
	{
		_activityImage					= image;
		_activityTitle					= title;
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	The label displaying the activity title.
 *
 *	@return	The label displaying the activity title.
 */
- (UILabel *)activityLabel
{
	if (!_activityLabel)
	{
		_activityLabel					= [[UILabel alloc] init];
		_activityLabel.font				= [UIFont easyJournalPreferredFontWithStyle:UIFontTextStyleBody];
		_activityLabel.text				= self.activityTitle;
		_activityLabel.textAlignment	= NSTextAlignmentCenter;
		
		[self.contentView addSubviewForAutoLayout:_activityLabel];
	}
	
	return _activityLabel;
}

/**
 *	A view displaying the thumbnail of the activity being displayed.
 *
 *	@return	A view displaying the thumbnail of the activity being displayed.
 */
- (UIImageView *)activityThumbnailView
{
	if (!_activityThumbnailView)
	{
		_activityThumbnailView				= [[UIImageView alloc] initWithImage:self.activityImage];
		_activityThumbnailView.contentMode	= UIViewContentModeScaleAspectFit;
		
		[self.contentView addSubviewForAutoLayout:_activityThumbnailView];
	}
	
	return _activityThumbnailView;
}

#pragma mark - Property Accessor Methods - Setters

/**
 *	An image symbolising the activity being displayed.
 *
 *	@param	activityImage				An image symbolising the activity being displayed.
 */
- (void)setActivityImage:(UIImage *)activityImage
{
	if (_activityImage == activityImage)
		return;
	
	_activityImage						= activityImage;
	
	self.activityThumbnailView.image	= _activityImage;
}

/**
 *	The title of the activity being displayed.
 *
 *	@param	activityTitle				The title of the activity being displayed.
 */
- (void)setActivityTitle:(NSString *)activityTitle
{
	if (_activityTitle == activityTitle)
		return;
	
	_activityTitle						= activityTitle;
	
	self.activityLabel.text				= _activityTitle;
}

@end