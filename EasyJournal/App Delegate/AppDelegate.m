//
//  AppDelegate.m
//  EasyJournal
//
//  Created by James Valaitis on 03/09/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityNavigationController.h"
#import "AppDelegate.h"
#import "GeneralActivityViewController.h"
#import "ActivityNavigationRootItem.h"

#pragma mark - App Delegate Implementation

@implementation AppDelegate

#pragma mark - UIApplicationDelegate Methods

/**
 *	Tells the delegate that the launch process is almost done and the app is almost ready to run.
 *
 *	@param	application					The delegating application object.
 *	@param	launchOptions				A dictionary indicating the reason the application was launched (if any).
 *
 *	@return	NO if the application cannot handle the URL resource, otherwise return YES.
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	//	set up the window and customise it
    self.window							= [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor			= [UIColor whiteColor];
	self.window.tintColor				= [UIColor easyJournalRed];
	[UIApplication sharedApplication].statusBarStyle	= UIStatusBarStyleLightContent;
	
	//	sort out view controllers
	GeneralActivityViewController *activityVC	= [[GeneralActivityViewController alloc] init];
	UIImage *rootItemImage						= [UIImage imageNamed:@"rootItemI"];
	ActivityNavigationRootItemOption *optionA	= [[ActivityNavigationRootItemOption alloc] initWithRootOptionImage:rootItemImage
																					  andRootOptionViewController:activityVC];
	ActivityNavigationRootItem *rootItem		= [[ActivityNavigationRootItem alloc] initWithRootOptions:@[optionA]];
	ActivityNavigationController *navigation	= [[ActivityNavigationController alloc] initWithRootNavigationItem:rootItem];
	self.window.rootViewController				= navigation;
	
    [self.window makeKeyAndVisible];
	
    return YES;
}

@end