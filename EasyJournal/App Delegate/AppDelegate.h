//
//  AppDelegate.h
//  EasyJournal
//
//  Created by James Valaitis on 03/09/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#pragma mark - App Delegate Public Interface

@interface AppDelegate : UIResponder <UIApplicationDelegate>

#pragma mark - Public Properties

/**	The window to use when presenting a storyboard.	*/
@property (nonatomic, strong)	UIWindow	*window;

@end