//
//  ActivityNavigationRootItem.h
//  EasyJournal
//
//  Created by James Valaitis on 02/02/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationRootItemOption.h"

#pragma mark - Activity Navigation Root Item Public Interface

@interface ActivityNavigationRootItem : NSObject {}

#pragma mark - Public Properties

/**	The options to be displayed in this root item, in the order they will be presented.	*/
@property (nonatomic, strong, readonly)		NSArray		*rootOptions;

#pragma mark - Public Methods

/**
 *	Initialises a root navigation item with each option in the item.
 *
 *	@param	rootOptions					An array of ActivityNavigationRootItemOptions to be displayed in the root item, in the order they will be presented.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootOptions:(NSArray *)rootOptions;

@end