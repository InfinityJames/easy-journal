//
//  ActivityNavigationRootItem.m
//  EasyJournal
//
//  Created by James Valaitis on 02/02/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationRootItem.h"

#pragma mark - Activity Navigation Root Item Private Class Extension

@interface ActivityNavigationRootItem () {}

/**	The options to be displayed in this root item, in the order they will be presented.	*/
@property (nonatomic, strong, readwrite)	NSArray		*rootOptions;

@end

#pragma mark - Activity Navigation Root Item Implementation

@implementation ActivityNavigationRootItem {}

#pragma mark - Initialisation

/**
 *	Initialises a root navigation item with each option in the item.
 *
 *	@param	rootOptions					An array of ActivityNavigationRootItemOptions to be displayed in the root item, in the order they will be presented.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootOptions:(NSArray *)rootOptions
{
	if (self = [super init])
	{
		_rootOptions					= rootOptions;
	}
	
	return self;
}

@end