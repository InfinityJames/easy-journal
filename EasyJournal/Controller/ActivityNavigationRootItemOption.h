//
//  ActivityNavigationRootItemOption.h
//  EasyJournal
//
//  Created by James Valaitis on 02/02/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

@class ActivityViewController;

#pragma mark - Activity Navigation Root Item Option Public Interface

@interface ActivityNavigationRootItemOption : NSObject {}

#pragma mark - Public Properties

/**	The image representing this option.	*/
@property (nonatomic, strong, readonly)	UIImage					*optionImage;
/**	The view controller for this option.	*/
@property (nonatomic, strong, readonly)	ActivityViewController	*optionViewController;

#pragma mark - Public Methods

/**
 *	Initialises an option item to be used at the root of navigation items view.
 *
 *	@param	optionImage					The image for this option.
 *	@param	optionViewController		The view controller for this option.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootOptionImage:(UIImage *)optionImage
			andRootOptionViewController:(ActivityViewController *)optionViewController;

@end