//
//  ActivityNavigationItem.m
//  EasyJournal
//
//  Created by James Valaitis on 25/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationDataSource.h"
#import "ActivityNavigationItem.h"
#import "ActivityProtocol.h"

#pragma mark - Activity Navigation Item Private Class Extension

@interface ActivityNavigationItem () {}

#pragma mark - Private Properties

/**	The data source for the acitivity items view in the navigation controller.	*/
@property (nonatomic, weak, readwrite)	ActivityNavigationDataSource	*activityNavigationDataSource;

@end

#pragma mark - Activity Navigation Item Implementation

@implementation ActivityNavigationItem {}

#pragma mark - Initialisation

/**
 *	Initialises a navigation item to be used for the view controllers with the data source.
 *
 *	@param	dataSource					The data source for the navigaton view.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithDataSource:(ActivityNavigationDataSource *)dataSource
{
	if (self = [super init])
	{
		_activityNavigationDataSource = dataSource;
	}
	
	return self;
}

#pragma mark - Navigation View

/**
 *	Adds an activity as to the navigation view.
 *
 *	@param	activity					The activity item to be displayed on the navigation view.
 */
- (void)pushActivityOntoNavigationView:(id <ActivityProtocol>)activity
{
	[self.activityNavigationDataSource pushActivityAsCell:activity];
}

@end