//
//  ActivityRootItemOptionSelectionViewController.m
//  EasyJournal
//
//  Created by James Valaitis on 06/04/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityCell.h"
#import "ActivityNavigationRootItemOptionSelectionViewController.h"
#import "ActivityNavigationRootItem.h"
#import "ArrayDataSource.h"

#pragma mark - Constants & Static Variables

/**	The reuse identifier for cells in the collection view allowing the user to select an option.	*/
static NSString *const ActivityNavigationRootItemOptionSelectionCellIdentifier		= @"optionSelectionCellIdentifier";
/**	The dimension of a root item option when displayed in the collection view.	*/
static CGFloat const ActivityNavigationRootItemOptionSelectionViewControllerRootOptionItemDimension = 64.0f;

#pragma mark - Activity Root Item Option Selection View Controller Private Class Extension

@interface ActivityNavigationRootItemOptionSelectionViewController () <ArrayDataSourceDelegate, UICollectionViewDelegate>

#pragma mark - Private Properties

/**	The data source for the selection collection view.	*/
@property (nonatomic, strong)	ArrayDataSource		*collectionViewDataSource;
/**	A collection view displaying the options.	*/
@property (nonatomic, strong)	UICollectionView	*selectionCollectionView;
/**	The options to offer the user.	*/
@property (nonatomic, strong)	NSArray				*rootItemOptions;

@end

#pragma mark - Activity Root Item Option Selection View Controller Implementation

@implementation ActivityNavigationRootItemOptionSelectionViewController

#pragma mark - ArrayDataSourceDelegate Methods

/**
 *	Used to configure a cell with a given item.
 *
 *	@param	arrayDataSource				The array data source calling for this configuration.
 *	@param	cell						The table view / collection view cell requiring configuration.
 *	@param	item						The item associated with the given cell.
 *	@param	indexPath					The index path of the given cell.
 */
- (void)arrayDataSource:(ArrayDataSource *)arrayDataSource
		  configureCell:(id)cell
			   withItem:(id)item
			atIndexPath:(NSIndexPath *)indexPath
{
	ActivityCell *activityCell = cell;
	ActivityNavigationRootItemOption *option = item;
	
	activityCell.activityImage = option.optionImage;
}

#pragma mark - Initialisation

/**
 *	Initialises this option selection view controller with the root item to present.
 *
 *	@param	rootItem					The root item with the option to offer for selection.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootItem:(ActivityNavigationRootItem *)rootItem
{
	if (self = [super init])
	{
		_rootItemOptions = rootItem.rootOptions;
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	The data source for the selection collection view.
 *
 *	@return	The data source for the selection collection view.
 */
- (ArrayDataSource *)collectionViewDataSource
{
	if (!_collectionViewDataSource)
	{
		_collectionViewDataSource = [[ArrayDataSource alloc] initWithItems:self.rootItemOptions
															cellIdentifier:ActivityNavigationRootItemOptionSelectionCellIdentifier andDelegate:self];
	}
	
	return _collectionViewDataSource;
}

/**
 *	A collection view displaying the options.
 *
 *	@return	A collection view displaying the options.
 */
- (UICollectionView *)selectionCollectionView
{
	//	lazy instantiation
	if (!_selectionCollectionView)
	{
		//	create the layout for the view
		UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
		CGFloat itemDimension = ActivityNavigationRootItemOptionSelectionViewControllerRootOptionItemDimension;
		CGFloat itemSpacing = ActivityNavigationRootItemOptionSelectionViewControllerRootOptionItemDimension / 2.0f;
		collectionViewLayout.itemSize = CGSizeMake(itemDimension, itemDimension);
		collectionViewLayout.minimumInteritemSpacing = itemSpacing;
		
		//	create the collection view
		_selectionCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
													  collectionViewLayout:collectionViewLayout];
		_selectionCollectionView.backgroundColor = [UIColor clearColor];
		_selectionCollectionView.contentInset = UIEdgeInsetsMake(itemSpacing, itemSpacing, itemSpacing, itemSpacing);
		_selectionCollectionView.dataSource = self.collectionViewDataSource;
		_selectionCollectionView.delegate = self;
		[_selectionCollectionView registerClass:[ActivityCell class]
					 forCellWithReuseIdentifier:ActivityNavigationRootItemOptionSelectionCellIdentifier];
	}
	
	return _selectionCollectionView;
}

#pragma mark - Subview Management

/**
 *	Adds the subviews and positions them with autolayout.
 */
- (void)addAndPositionSubviews
{
	//	add subviews
	[self.view addSubviewForAutoLayout:self.selectionCollectionView];
	
	//	set up auto layout
	NSDictionary *viewsDictionary = @{@"collectionView"		: self.selectionCollectionView};
	
	//	create and add the constraints
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
																	  options:kNilOptions
																	  metrics:nil
																		views:viewsDictionary]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
																	  options:kNilOptions
																	  metrics:nil
																		views:viewsDictionary]];
}

#pragma mark - UICollectionViewDelegate Methods

/**
 *	Tells the delegate that the item at the specified index path was selected.
 *
 *	@param	collectionview				The collection view object that is notifying you of the selection change.
 *	@param	indexPath					The index path of the cell that was selected.
 */
- (void)  collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	ActivityNavigationRootItemOption *selectedOption = [self.collectionViewDataSource itemForItemAtIndexPath:indexPath];
	[self.delegate activityNavigationRootItemOptionSelectionViewController:self didSelectOption:selectedOption];
}

#pragma mark - View Lifecycle

/**
 *	Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.view.backgroundColor = [UIColor clearColor];
	
	[self addAndPositionSubviews];
}

@end