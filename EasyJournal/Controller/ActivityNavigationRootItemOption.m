//
//  ActivityNavigationRootItemOption.m
//  EasyJournal
//
//  Created by James Valaitis on 02/02/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

#import "ActivityNavigationRootItemOption.h"
#import "ActivityViewController.h"

#pragma mark - Activity Navigation Root Item Option Private Class Extension

@interface ActivityNavigationRootItemOption () {}

#pragma mark - Private Properties

/**	The image representing this option.	*/
@property (nonatomic, strong, readwrite)	UIImage					*optionImage;
/**	The image representing this option.	*/
@property (nonatomic, strong, readwrite)	ActivityViewController	*optionViewController;

@end

#pragma mark - Activity Navigation Root Item Option Implementation

@implementation ActivityNavigationRootItemOption {}

#pragma mark - Initialisation

/**
 *	Initialises an option item to be used at the root of navigation items view.
 *
 *	@param	optionImage					The image for this option.
 *	@param	optionViewController		The view controller for this option.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootOptionImage:(UIImage *)optionImage
			andRootOptionViewController:(ActivityViewController *)optionViewController
{
	if (self = [super init])
	{
		_optionImage					= optionImage;
		_optionViewController			= optionViewController;
	}
	
	return self;
}

@end