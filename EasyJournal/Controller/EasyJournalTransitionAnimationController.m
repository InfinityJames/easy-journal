//
//  EasyJournalTransitionAnimationController.m
//  EasyJournal
//
//  Created by James Valaitis on 05/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityProtocol.h"
#import "EasyJournalAnimatableProtocol.h"
#import "EasyJournalTransitionAnimationController.h"

#pragma mark - Easy Journal Transition Animation Controller Private Class Extension

@interface EasyJournalTransitionAnimationController () {}

#pragma mark - Private Properties

/**	An object that dynamically animates views.	*/
@property (nonatomic, strong)		UIDynamicAnimator		*animator;
/**	An object that adds gravity to any views in the transition that need to fall off screen.	*/
@property (nonatomic, strong)		UIGravityBehavior		*gravityBehaviour;

@end

#pragma mark - Easy Journal Transition Animation Controller Implementation

@implementation EasyJournalTransitionAnimationController {}

#pragma mark - Animating Transitions

/**
 *	Performs a custom view controller pop animation.
 *
 *	@param	transitionContext			The context object containing information about the transition.
 */
- (void)animatePopWithTransitionContext:(id <UIViewControllerContextTransitioning>)transitionContext
{
	//	obtain the the state frome the transition context.
	UIView *containerView, *fromView, *toView;
	UIViewController *fromViewController, *toViewController;
	
	[self getContainerView:&containerView
				  fromView:&fromView
					toView:&toView
		fromViewController:&fromViewController
		  toViewController:&toViewController
	  forTransitionContext:transitionContext];
	
	//	retrieve the view to keep and the views to dismiss, as well as other information
	UIViewController<EasyJournalAnimatableProtocol> *animatableViewController	= (UIViewController<EasyJournalAnimatableProtocol> *)fromViewController;
	NSArray *viewsToDismiss				= [animatableViewController viewsToDismiss];
	
	//	set initial state
	[containerView addSubview:toView];
	[containerView bringSubviewToFront:fromView];
	[containerView jv_addSubviews:[[NSSet alloc] initWithArray:viewsToDismiss]];
	containerView.clipsToBounds			= NO;
	[containerView.superview bringSubviewToFront:containerView];
	toView.alpha						= 0.0f;
	
	//	add gravity to the views that need dismissing
	self.gravityBehaviour				= [[UIGravityBehavior alloc] initWithItems:viewsToDismiss];
	for (UIView *viewToDismiss in viewsToDismiss)
	{
		[self.animator addBehavior:[self randomPushBehaviourForView:viewToDismiss]];
	}
	[self.animator addBehavior:self.gravityBehaviour];
	
	//	animate it all
	NSTimeInterval animationDuration	= [self transitionDuration:transitionContext];
	
	[UIView animateKeyframesWithDuration:animationDuration
								   delay:0.0f
								 options:kNilOptions
							  animations:
	 ^{
		 [UIView addKeyframeWithRelativeStartTime:0.0f
								 relativeDuration:animationDuration / 2.0f
									   animations:
		  ^{
			  //	rotate the from view
			  fromView.alpha			= 0.0f;
		  }];
		 
		 [UIView addKeyframeWithRelativeStartTime:animationDuration / 2.0f
								 relativeDuration:animationDuration / 2.0f
									   animations:
		  ^{
			  //	rotate the from view
			  toView.alpha				= 1.0f;
		  }];
		 
		 [UIView addKeyframeWithRelativeStartTime:0.0f
								 relativeDuration:animationDuration
									   animations:
		  ^{
			  //	rotate the from view
			  fromView.frame			= CGRectOffset(fromView.frame, CGRectGetWidth(fromView.frame), 0.0f);
		  }];
	 }
							  completion:^(BOOL finished)
	 {
		 [viewsToDismiss makeObjectsPerformSelector:@selector(removeFromSuperview)];
		 
		 [fromView removeFromSuperview];
		 [self completeTransition:transitionContext withActivity:nil];
	 }];
}

/**
 *	Performs a custom view controller presentation animation.
 *
 *	@param	transitionContext			The context object containing information about the transition.
 */
- (void)animatePresentationWithTransitionContext:(id <UIViewControllerContextTransitioning>)transitionContext
{
	//	obtain the the state frome the transition context.
	UIView *containerView, *fromView, *toView;
	UIViewController *fromViewController, *toViewController;
	
	[self getContainerView:&containerView
				  fromView:&fromView
					toView:&toView
		fromViewController:&fromViewController
		  toViewController:&toViewController
	  forTransitionContext:transitionContext];
	
	//	retrieve the view to keep and the views to dismiss, as well as other information
	UIViewController<EasyJournalAnimatableProtocol> *animatableViewController	= (UIViewController<EasyJournalAnimatableProtocol> *)fromViewController;
	UIView *viewToKeep					= [animatableViewController viewToKeep];
	NSArray *viewsToDismiss				= [animatableViewController viewsToDismiss];
	CGRect destinationFrame				= [animatableViewController destinationFrameForView:viewToKeep];
	id<ActivityProtocol> activity		= [animatableViewController activityForView:viewToKeep];
	
	//	set initial state
	[containerView addSubview:toView];
	[containerView.superview.superview addSubview:viewToKeep];
	[containerView jv_addSubviews:[[NSSet alloc] initWithArray:viewsToDismiss]];
	containerView.clipsToBounds			= NO;
	[containerView.superview bringSubviewToFront:containerView];
	toView.alpha						= 0.0f;
	toView.frame						= CGRectOffset(fromView.frame, fromView.bounds.size.width, 0.0f);
	
	//	add gravity to the views that need dismissing
	self.gravityBehaviour				= [[UIGravityBehavior alloc] initWithItems:viewsToDismiss];
	for (UIView *viewToDismiss in viewsToDismiss)
	{
		[self.animator addBehavior:[self randomPushBehaviourForView:viewToDismiss]];
	}
	[self.animator addBehavior:self.gravityBehaviour];
	
	//	animate it all
	NSTimeInterval animationDuration	= [self transitionDuration:transitionContext];
	
	[UIView animateKeyframesWithDuration:animationDuration
								   delay:0.0f
								 options:kNilOptions
							  animations:
	^{
		[UIView addKeyframeWithRelativeStartTime:0.0f
								relativeDuration:animationDuration / 2.0f
									  animations:
		^{
			//	rotate the from view
			fromView.alpha				= 0.0f;
			
			viewToKeep.frame			= destinationFrame;
		}];
		
		[UIView addKeyframeWithRelativeStartTime:animationDuration / 2.0f
								 relativeDuration:animationDuration / 2.0f
									   animations:
		  ^{
			  //	rotate the from view
			  toView.alpha				= 1.0f;
		  }];
		
		[UIView addKeyframeWithRelativeStartTime:0.0f
								relativeDuration:animationDuration
									  animations:
		 ^{
			 //	rotate the from view
			 toView.frame				= fromView.frame;
		 }];
	 }
							  completion:^(BOOL finished)
	 {
		 [viewsToDismiss makeObjectsPerformSelector:@selector(removeFromSuperview)];
		 
		 [fromView removeFromSuperview];
		 [viewToKeep performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.5f];
		 [self completeTransition:transitionContext withActivity:activity];
	 }];
}

#pragma mark - Initialisation

/**
 *	Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
 *
 *	@return	An initialized object.
 */
- (instancetype)init
{
	return [self initWithCompletionHandler:nil];
}

/**
 *	Initialise an animation controller with a block to call upon completion.
 *
 *	@param	completion					The block to call upon the completion of any transition controlled.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithCompletionHandler:(EasyJournalAnimationCompletion)completion
{
	if (self = [super init])
	{
		_completionHandler				= [completion copy];
	}
	
	return self;
}

#pragma mark - Utility Methods: Dynamics

/**
 *	Creates a push behaviour for the given view.
 *
 *	@return	A UIPushBehavior attached to the given view.
 */
- (UIPushBehavior *)randomPushBehaviourForView:(UIView *)view
{
	UIPushBehavior *pushBehaviour		= [[UIPushBehavior alloc] initWithItems:@[view] mode:UIPushBehaviorModeInstantaneous];
	//	make the angle random, but always downwards
	pushBehaviour.angle					= (arc4random() % 180) * (M_PI / 180.0f);
	pushBehaviour.magnitude				= (arc4random() % 100) / 100.0f;
	
	return pushBehaviour;
}

#pragma mark - Utility Methods: Transition

/**
 *	Handles the completion of an animated transition with the given activity object.
 *
 *	@param	transitionContext			The context object containing information about the transition.
 *	@param	activity					An object which conform to the ActivityProtocol which was key to the transition
 */
- (void)completeTransition:(id <UIViewControllerContextTransitioning>)transitionContext
			  withActivity:(id<ActivityProtocol>)activity
{
	if (self.completionHandler)
		self.completionHandler(activity);
	
	//	can't use ternary operation on object conforming to a protocol
	id noProtocol						= activity;
	NSDictionary *userInfo				= @{EasyJournalTransitionAnimationCompletedUserInfoActivityKey	: noProtocol ? : [NSNull null]};
	[[NSNotificationCenter defaultCenter] postNotificationName:EasyJournalTransitionAnimationCompletedNotification
														object:nil
													  userInfo:userInfo];
	
	//	complete transition
	[transitionContext completeTransition:![transitionContext transitionWasCancelled]];
}

/**
 *	Convenient way to get the from view, to view and the view controllers from a given transition context.
 *
 *	@param	containerView				The view that acts as the superview for the views involved in the transition.
 *	@param	fromView					The view to displayed at the beginning of the transition.
 *	@param	toView						The view visible at the end of a completed transition.
 *	@param	fromViewController			The view controller that is visible at the beginning of the transition.
 *	@param	toViewController			Identifies the view controller that is visible at the end of a completed transition.
 *	@param	transitionContext			The context object containing information about the transition.
 */
- (void)getContainerView:(UIView **)containerView
				fromView:(UIView **)fromView
				  toView:(UIView **)toView
	  fromViewController:(UIViewController **)fromViewController
		toViewController:(UIViewController **)toViewController
	forTransitionContext:(id <UIViewControllerContextTransitioning>)transitionContext
{
	//	obtain state from context
	*fromViewController					= [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
	*toViewController					= [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	*containerView						= [transitionContext containerView];
	*fromView							= (*fromViewController).view;
	*toView								= (*toViewController).view;
}

#pragma mark - UIViewControllerAnimatedTransitioning Methods

/**
 *	Performs a custom view controller transition animation.
 *
 *	@param	transitionContext			The context object containing information about the transition.
 */
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
	//	obtain state from context
	UIViewController *toViewController	= [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	UIViewController *fromViewController= [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
	
	//	make sure everything will work
	NSString *assertMessage				=  @"View Controller (%@) does not conform to the required protocol: %@";
	NSAssert([toViewController conformsToProtocol:@protocol(EasyJournalAnimatableProtocol)], assertMessage,
			 NSStringFromClass([toViewController class]),
			 NSStringFromProtocol(@protocol(EasyJournalAnimatableProtocol)));
	NSAssert([fromViewController conformsToProtocol:@protocol(EasyJournalAnimatableProtocol)], assertMessage,
			 NSStringFromClass([toViewController class]),
			 NSStringFromProtocol(@protocol(EasyJournalAnimatableProtocol)));
	
	//	set up the dynamic animator
	UIView *containerView				= [transitionContext containerView];
	self.animator						= [[UIDynamicAnimator alloc] initWithReferenceView:containerView];
	
	if (self.reverse)
		//	pop the view controller
		[self animatePopWithTransitionContext:transitionContext];
	else
		//	present the view controller
		[self animatePresentationWithTransitionContext:transitionContext];
}

/**
 *	Called when the system needs the duration, in seconds, of the transition animation.
 *
 *	@param	transitionContext			The context object containing information to use during the transition.
 *
 *	@return	The duration, in seconds, of your custom transition animation.
 */
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
	return 1.0f;
}

@end