//
//  SpecificActivitySelectionViewController.m
//  EasyJournal
//
//  Created by James Valaitis on 27/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "SpecificActivityLoader.h"
#import "SpecificActivitySelectionViewController.h"

#pragma mark - Specific Activity Selection VC Private Class Extension

@interface SpecificActivitySelectionViewController () {}

@end

#pragma mark - Specific Activity Selection VC Implementation

@implementation SpecificActivitySelectionViewController {}

#pragma mark - ArrayDataSourceDelegate Methods

/**
 *	Used to configure a cell with a given item.
 *
 *	@param	arrayDataSource				The array data source calling for this configuration.
 *	@param	cell						The table view / collection view cell requiring configuration.
 *	@param	item						The item associated with the given cell.
 *	@param	indexPath					The index path of the given cell.
 */
- (void)arrayDataSource:(ArrayDataSource *)arrayDataSource
		  configureCell:(id)cell
			   withItem:(id)item
			atIndexPath:(NSIndexPath *)indexPath
{
	[super arrayDataSource:arrayDataSource configureCell:cell withItem:item atIndexPath:indexPath];
	
	ActivityCell *activityCell			= cell;
	SpecificActivity *specificActivity	= item;
	
	activityCell.activityTitle			= specificActivity.title;
	activityCell.activityImage			= specificActivity.activityImage;
}

#pragma mark - Initialisation

/**
 *	Initialises a specific activity selection view controller with the type of specific activity to display.
 *
 *	@param	activityType				The type of the specific activities to display.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityType:(NSString *)activityType
{
	if (self = [super initWithActivities:[[[SpecificActivityLoader alloc] init] specificActivitiesOfType:activityType]])
	{
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	A unique identifier for a cell representing activities.
 *
 *	@return	A unique identifier for a cell representing activities.
 */
- (NSString *)cellIdentifier
{
	return @"SpecificCell";
}

#pragma mark - UITableViewDelegate Methods

/**
 *	Tells the delegate that the specified row is now selected.
 *
 *	@param	tableView					A table-view object informing the delegate about the new row selection.
 *	@param	indexPath					An index path locating the new selected row in tableView.
 */
- (void)	  tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[super tableView:tableView didSelectRowAtIndexPath:indexPath];
	
	SpecificActivity *specificActivity = self.selectedActivity;
	
	[self.activityNavigationController pushViewController:[[SpecificActivitySelectionViewController alloc] initWithActivityType:@""]
												 animated:YES
									withCompletionHandler:
	 ^{
		 if (specificActivity)
			 [self.activityNavigationItem pushActivityOntoNavigationView:specificActivity];
	 }];
}

@end