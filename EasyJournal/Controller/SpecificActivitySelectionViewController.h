//
//  SpecificActivitySelectionViewController.h
//  EasyJournal
//
//  Created by James Valaitis on 27/12/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivitySelectionViewController.h"

#pragma mark - Specific Activity Selection VC Private Class Extension

@interface SpecificActivitySelectionViewController : ActivitySelectionViewController {}

#pragma mark - Public Methods

/**
 *	Initialises a specific activity selection view controller with the type of specific activity to display.
 *
 *	@param	activityType				The type of the specific activities to display.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivityType:(NSString *)activityType;

@end