//
//  ActivityRootItemOptionSelectionViewController.h
//  EasyJournal
//
//  Created by James Valaitis on 06/04/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

@class ActivityNavigationRootItem;
@class ActivityNavigationRootItemOption;
@class ActivityNavigationRootItemOptionSelectionViewController;

#pragma mark - Activity Navigation Root Item Option Selection Delegate Declaration

@protocol ActivityNavigationRootItemOptionSelectionDelegate <NSObject>

#pragma mark - Required Methods

@required

/**
 *	Sent to the delegate when the user has selected an option.
 *
 *	@param	optionSelectionVC			The view controller sending this message.
 *	@param	rootItemOption				The selected option.
 */
- (void)activityNavigationRootItemOptionSelectionViewController:(ActivityNavigationRootItemOptionSelectionViewController *)optionSelectionVC
												didSelectOption:(ActivityNavigationRootItemOption *)rootItemOption;
/**
 *	Sent to the delegate when the user has cancel the selection operation.
 *
 *	@param	optionSelectionVC			The view controller sending this message.
 */
- (void)activityNavigationRootItemOptionSelectionViewControllerDidCancel:(ActivityNavigationRootItemOptionSelectionViewController *)optionSelectionVC;

@end

#pragma mark - Activity Root Item Option Selection View Controller Public Interface

@interface ActivityNavigationRootItemOptionSelectionViewController : UIViewController

#pragma mark - Public Properties

/**	An object interested in the selections made in this view controller.	*/
@property (nonatomic, weak)	id <ActivityNavigationRootItemOptionSelectionDelegate>		delegate;

#pragma mark - Public Methods

/**
 *	Initialises this option selection view controller with the root item to present.
 *
 *	@param	rootItem					The root item with the option to offer for selection.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootItem:(ActivityNavigationRootItem *)rootItem;

@end