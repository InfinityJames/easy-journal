//
//  ActivitySelectionViewController.h
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

//	these headers are imported publicly because all subclasses should know about them
#import "ActivityCell.h"
#import "ActivityProtocol.h"
#import "ActivityViewController.h"
#import "ArrayDataSource.h"
#import "EasyJournalAnimatableProtocol.h"

#pragma mark - Activity Selection View Controller Public Interface

@interface ActivitySelectionViewController : ActivityViewController <ArrayDataSourceDelegate, UITableViewDelegate> {}

#pragma mark - Public Properties

/**	An array of objects that conform to the ActivityProtocol.	*/
@property (nonatomic, strong)			NSArray					*activities;
/**	The data source to be used for the UITableView.	*/
@property (nonatomic, strong)			ArrayDataSource			*arrayDataSource;
/**	A unique identifier for a cell representing activities.	*/
@property (nonatomic, strong, readonly)	NSString				*cellIdentifier;
/**	The activity selected by the user.	*/
@property (nonatomic, strong)			id <ActivityProtocol>	selectedActivity;
/**	The index path of the selected activity.	*/
@property (nonatomic, strong)			NSIndexPath				*selectedIndexPath;
/**	The table view that holds the current selections.	*/
@property (nonatomic, strong, readonly)	UITableView				*selectionsTableView;

#pragma mark - Public Methods

/**
 *	Initialises this activity selection view controller with the activities that can be selected.
 *
 *	@param	activities					An array of objects that conform to the ActivityProtocol.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivities:(NSArray *)activities;

@end