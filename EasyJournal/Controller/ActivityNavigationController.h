//
//  ActivityNavigationController.h
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

@class ActivityNavigationDataSource;
@class ActivityNavigationRootItem;
@class ActivityViewController;

#pragma mark - Activity Navigation Controller Public Interface

@interface ActivityNavigationController : UIViewController {}

#pragma mark - Public Properties

/**	The data source for the acitivity items view in the navigation controller.	*/
@property (nonatomic, strong, readonly)	ActivityNavigationDataSource	*activityNavigationDataSource;

#pragma mark - Public Methods : Initialisation

/**
 *	Initialised an instance of this activity navigation controller. (Designated initialiser).
 *
 *	@param	rootNavigationItem			The root item for the navigation items view.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootNavigationItem:(ActivityNavigationRootItem *)rootNavigationItem;

#pragma mark - Public Methods: Navigation

/**
 *	Pops the top view controller from the navigation stack and updates the display.
 *
 *	@param	animated					Set this value to YES to animate the transition. Pass NO otherwise.
 */
- (void)popViewControllerAnimated:(BOOL)animated;
/**
 *	Pushes a view controller onto the receiver’s stack and updates the display.
 *
 *	@param	viewController				The view controller that is pushed onto the stack.
 *	@param	animated					Specify YES to animate the transition or NO if you do not want the transition to be animated.
 */
- (void)pushViewController:(ActivityViewController *)viewController
				  animated:(BOOL)animated;
/**
 *	Pushes a view controller onto the receiver’s stack and updates the display.
 *
 *	@param	viewController				The view controller that is pushed onto the stack.
 *	@param	animated					Specify YES to animate the transition or NO if you do not want the transition to be animated.
 *	@param	completionHandler			A block called upon the completion of the pushing of the given view controller.
 */
- (void)pushViewController:(ActivityViewController *)viewController
				  animated:(BOOL)animated
	 withCompletionHandler:(void(^)())completionHandler;

@end

#import "ActivityNavigationController+Utilities.h"