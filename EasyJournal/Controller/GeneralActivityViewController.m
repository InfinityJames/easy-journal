//
//  GeneralActivityViewController.m
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityCell.h"
#import "GeneralActivityLoader.h"
#import "GeneralActivityViewController.h"
#import "SpecificActivitySelectionViewController.h"

#pragma mark - General Activity View Controller Private Class Extension

@interface GeneralActivityViewController () {}

/**	An array of GeneralActivity objects.	*/
@property (nonatomic, strong)	NSArray			*generalActivities;

@end

#pragma mark - General Activity View Controller Implementation

@implementation GeneralActivityViewController {}

#pragma mark - ArrayDataSourceDelegate Methods

/**
 *	Used to configure a cell with a given item.
 *
 *	@param	arrayDataSource				The array data source calling for this configuration.
 *	@param	cell						The table view / collection view cell requiring configuration.
 *	@param	item						The item associated with the given cell.
 *	@param	indexPath					The index path of the given cell.
 */
- (void)arrayDataSource:(ArrayDataSource *)arrayDataSource
		  configureCell:(id)cell
			   withItem:(id)item
			atIndexPath:(NSIndexPath *)indexPath
{
	[super arrayDataSource:arrayDataSource configureCell:cell withItem:item atIndexPath:indexPath];
	
	ActivityCell *activityCell			= cell;
	GeneralActivity *generalActivity	= item;
	
	activityCell.activityTitle			= generalActivity.title;
	activityCell.activityImage			= generalActivity.activityImage;
}

#pragma mark - Initialisation

/**
 *	Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
 *
 *	@return	An initialized object.
 */
- (instancetype)init
{
	if (self = [super initWithActivities:[GeneralActivityLoader sharedInstance].generalActivities])
	{
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	A unique identifier for a cell representing activities.
 *
 *	@return	A unique identifier for a cell representing activities.
 */
- (NSString *)cellIdentifier
{
	return @"GeneralCell";
}

#pragma mark - UITableViewDelegate Methods

/**
 *	Tells the delegate that the specified row is now selected.
 *
 *	@param	tableView					A table-view object informing the delegate about the new row selection.
 *	@param	indexPath					An index path locating the new selected row in tableView.
 */
- (void)	  tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[super tableView:tableView didSelectRowAtIndexPath:indexPath];
	
	GeneralActivity *generalActivity = self.selectedActivity;
	SpecificActivitySelectionViewController *specificActivityVC	= [[SpecificActivitySelectionViewController alloc] initWithActivityType:generalActivity.activityType];
	
	[self.activityNavigationController pushViewController:specificActivityVC
												 animated:YES
									withCompletionHandler:
	^{
		if (generalActivity)
			[self.activityNavigationItem pushActivityOntoNavigationView:generalActivity];
	}];
}

@end