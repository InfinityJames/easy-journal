//
//  ActivityViewController.h
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityNavigationController.h"
#import "ActivityNavigationItem.h"

#pragma mark - Activity View Controller Public Interface

@interface ActivityViewController : UIViewController {}

#pragma mark - Public Properties

/**	The nearest ancestor in the view controller hierarchy that is a navigation controller.	*/
@property (nonatomic, weak)		ActivityNavigationController	*activityNavigationController;
/**	Used to control the navigation view from the view controller.	*/
@property (nonatomic, weak)		ActivityNavigationItem			*activityNavigationItem;

@end