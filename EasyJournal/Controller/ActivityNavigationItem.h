//
//  ActivityNavigationItem.h
//  EasyJournal
//
//  Created by James Valaitis on 25/01/2014.
//  Copyright (c) 2014 &Beyond. All rights reserved.
//

@class ActivityNavigationDataSource;
@protocol ActivityProtocol;

#pragma mark - Activity Navigation Item Public Interface

@interface ActivityNavigationItem : NSObject {}

#pragma mark - Public Properties

/**	The data source for the acitivity items view in the navigation controller.	*/
@property (nonatomic, weak, readonly)	ActivityNavigationDataSource	*activityNavigationDataSource;

#pragma mark - Public Methods

/**
 *	Initialises a navigation item to be used for the view controllers with the data source.
 *
 *	@param	dataSource					The data source for the navigaton view.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithDataSource:(ActivityNavigationDataSource *)dataSource;

/**
 *	Adds an activity as to the navigation view.
 *
 *	@param	activity					The activity item to be displayed on the navigation view.
 */
- (void)pushActivityOntoNavigationView:(id <ActivityProtocol>)activity;

@end