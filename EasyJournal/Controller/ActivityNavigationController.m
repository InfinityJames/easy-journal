//
//  ActivityNavigationController.m
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityProtocol.h"
#import "ActivityNavigationController.h"
#import "ActivityNavigationDataSource.h"
#import "ActivityNavigationDelegate.h"
#import "ActivityNavigationItem.h"
#import "ActivityNavigationRootItem.h"
#import "ActivityViewController.h"

#pragma mark - Constants & Static Variables

/**	Width of the border around the view.	*/
static CGFloat const ActivityNavigationControllerBorderWidth				= 4.0f;
/**	Height of the activity item collection view.	*/
static CGFloat const ActivityNavigationControllerCollectionViewHeight		= 128.0f;

#pragma mark - Activity Navigation Controller Private Class Extension

@interface ActivityNavigationController () <UICollectionViewDelegate> {}

#pragma mark - Private Properties: Navigation

/**	The navigation controller for the activity view controllers.	*/
@property (nonatomic, strong)				UINavigationController			*activityNavigationController;
/**	The data source for the acitivity items view in the navigation controller.	*/
@property (nonatomic, strong, readwrite)	ActivityNavigationDataSource	*activityNavigationDataSource;
/**	The delegate for the navigation controller.	*/
@property (nonatomic, strong)				ActivityNavigationDelegate		*activityNavigationDelegate;
/**	Used to control the navigation view from the view controller.	*/
@property (nonatomic, strong)				ActivityNavigationItem			*activityNavigationItem;
/**	The root item of the navigation view.	*/
@property (nonatomic, strong)				ActivityNavigationRootItem		*activityNavigationRootItem;
/**	The root view controller being presented in the navigation controller.	*/
@property (nonatomic, weak)					ActivityViewController			*rootViewController;

#pragma mark - Private Properties: State

/**	Whether or not a push / pop transition is urrently in progress.	*/
@property (nonatomic, assign)				BOOL							transitionInProgress;

#pragma mark - Private Properties: Subviews

/**	The view presenting the currently selected activity items.	*/
@property (nonatomic, strong)				UICollectionView				*activityItemsCollectionView;
/**	A view to be placed underneath the status bar.	*/
@property (nonatomic, strong)				UIView							*statusBarBackgroundView;

@end

#pragma mark - Activity Navigation Controller Implementation

@implementation ActivityNavigationController {}

#pragma mark - Autolayout

/**
 *	Positions the views with NSLayoutConstraints.
 */
- (void)addConstraints
{
	static CGFloat statusBarHeight = 20.0f;
	
	//	position view
	NSDictionary *viewsDictionary = @{@"items"				: self.activityItemsCollectionView,
									  @"navigation"			: self.activityNavigationController.view,
									  @"statusBackground"	: self.statusBarBackgroundView,
									  @"top"				: self.topLayoutGuide			};
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[items]|"
																	  options:kNilOptions
																	  metrics:nil
																		views:viewsDictionary]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.statusBarBackgroundView
														  attribute:NSLayoutAttributeTop
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.topLayoutGuide
														  attribute:NSLayoutAttributeBottom
														 multiplier:1.0f
														   constant:-statusBarHeight]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[statusBackground(status)][items(cvHeight)][navigation]|"
																	  options:NSLayoutAttributeLeading | NSLayoutAttributeTrailing
																	  metrics:@{@"cvHeight"	: @(ActivityNavigationControllerCollectionViewHeight),
																				@"status"	: @(statusBarHeight)}
																		views:viewsDictionary]];
	
	[self.view sendSubviewToBack:self.activityItemsCollectionView];
}

#pragma mark - Initialisation

/**
 *	Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
 *
 *	@return	An initialized object.
 */
- (instancetype)init
{
	return [self initWithRootNavigationItem:nil];
}

/**
 *	Initialised an instance of this activity navigation controller. (Designated initialiser).
 *
 *	@param	rootNavigationItem			The root item for the navigation items view.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithRootNavigationItem:(ActivityNavigationRootItem *)rootNavigationItem
{
	if (self = [super init])
	{
		//	get the root view controller out of first option
		_activityNavigationRootItem = rootNavigationItem;
		ActivityNavigationRootItemOption *option = _activityNavigationRootItem.rootOptions[0];
		_rootViewController = option.optionViewController;
		_rootViewController.activityNavigationController = self;
		
		//	initialise navigation controller and set delegate (we have strong reference to delegate)
		_activityNavigationController = [[UINavigationController alloc] initWithRootViewController:_rootViewController];
		
		//	register to know about when the transition completes
		[[NSNotificationCenter defaultCenter] addObserverForName:EasyJournalTransitionAnimationCompletedNotification
														  object:nil
														   queue:[NSOperationQueue mainQueue]
													  usingBlock:^(NSNotification *notification)
		 {
			 id <ActivityProtocol> activity = notification.userInfo[EasyJournalTransitionAnimationCompletedUserInfoActivityKey];
			 NSLog(@"Activity: %@", activity);
			 self.transitionInProgress = NO;
		 }];
	}
	
	return self;
}

#pragma mark - Navigation Stack Handling

/**
 *	Pops the top view controller from the navigation stack and updates the display.
 *
 *	@param	animated					Set this value to YES to animate the transition. Pass NO otherwise.
 */
- (void)popViewControllerAnimated:(BOOL)animated
{
	if (self.transitionInProgress) return;
	self.transitionInProgress = YES;
	
	[self.activityNavigationController popViewControllerAnimated:animated];
}

/**
 *	Pushes a view controller onto the receiver’s stack and updates the display.
 *
 *	@param	viewController				The view controller that is pushed onto the stack.
 *	@param	animated					Specify YES to animate the transition or NO if you do not want the transition to be animated.
 */
- (void)pushViewController:(ActivityViewController *)viewController
				  animated:(BOOL)animated
{
	[self pushViewController:viewController animated:animated withCompletionHandler:nil];
}

/**
 *	Pushes a view controller onto the receiver’s stack and updates the display.
 *
 *	@param	viewController				The view controller that is pushed onto the stack.
 *	@param	animated					Specify YES to animate the transition or NO if you do not want the transition to be animated.
 *	@param	completionHandler			A block called upon the completion of the pushing of the given view controller.
 */
- (void)pushViewController:(ActivityViewController *)viewController
				  animated:(BOOL)animated
	 withCompletionHandler:(void(^)())completionHandler
{
	if (self.transitionInProgress) return;
	self.transitionInProgress = YES;
	
	viewController.activityNavigationController = self;
	viewController.activityNavigationItem = self.activityNavigationItem;
	self.activityNavigationDelegate.completionHandler = completionHandler;

	[self.activityNavigationController pushViewController:viewController animated:animated];
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	The view presenting the currently selected activity items.
 *
 *	@return	The view presenting the currently selected activity items.
 */
- (UICollectionView *)activityItemsCollectionView
{
	if (!_activityItemsCollectionView)
	{
		//	figure out how to position items in the view
		CGFloat itemSizeDimension = ActivityNavigationControllerCollectionViewHeight / 2.0f;
		CGFloat contentInsetDimension = ActivityNavigationControllerCollectionViewHeight / 4.0f;
		
		//	initialise and configure the layout
		UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
		flowLayout.itemSize = CGSizeMake(itemSizeDimension, itemSizeDimension);
		flowLayout.minimumInteritemSpacing = contentInsetDimension / 2.0f;
		flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
		
		//	initialise and configure the collection view
		_activityItemsCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
		_activityItemsCollectionView.backgroundColor = [UIColor whiteColor];
		_activityItemsCollectionView.contentInset = UIEdgeInsetsMake(contentInsetDimension, contentInsetDimension,
																	 contentInsetDimension, contentInsetDimension);
		_activityItemsCollectionView.delegate = self;
		
		[self.view addSubviewForAutoLayout:_activityItemsCollectionView];
	}
	
	return _activityItemsCollectionView;
}

/**
 *	The delegate for the navigation controller.
 *
 *	@return	The delegate for the navigation controller.
 */
- (ActivityNavigationDelegate *)activityNavigationDelegate
{
	if (!_activityNavigationDelegate)
		_activityNavigationDelegate = [[ActivityNavigationDelegate alloc] init];
	
	return _activityNavigationDelegate;
}

/**
 *	A view to be placed underneath the status bar.
 *
 *	@return	A view to be placed underneath the status bar.
 */
- (UIView *)statusBarBackgroundView
{
	if (!_statusBarBackgroundView)
	{
		_statusBarBackgroundView = [[UIView alloc] init];
		_statusBarBackgroundView.backgroundColor = [UIColor easyJournalBlue];
		
		[self.view addSubviewForAutoLayout:_statusBarBackgroundView];
	}
	
	return _statusBarBackgroundView;
}

#pragma mark - UICollectionViewDelegate Methods

/**
 *	Tells the delegate that the item at the specified index path was selected.
 *
 *	@param	collectionview				The collection view object that is notifying you of the selection change.
 *	@param	indexPath					The index path of the cell that was selected.
 */
- (void)  collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.transitionInProgress || [self.activityNavigationDataSource indexPathIsLast:indexPath])
		return;
	self.transitionInProgress = YES;
	
	ActivityViewController *viewControllerToDisplay = (ActivityViewController *)self.activityNavigationController.viewControllers[indexPath.item];
	
	if (indexPath.item == 0)
	{
		[self.activityNavigationDataSource popAllActivities];
		[self.activityNavigationController popToRootViewControllerAnimated:YES];
	}
	else
	{
		[self.activityNavigationDataSource popBackToActivityAtIndex:indexPath.item];
		
		[self.activityNavigationController popToViewController:viewControllerToDisplay
													  animated:YES];
	}
}

#pragma mark - View Configuration & Management

/**
 *	Configures the view.
 */
- (void)configureView
{
	self.view.layer.borderColor = [UIColor easyJournalBlue].CGColor;
	self.view.layer.borderWidth = ActivityNavigationControllerBorderWidth;
}

/**
 *	The preferred status bar style for the view controller.
 *
 *	@return	A UIStatusBarStyle key indicating your preferred status bar style for the view controller.
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

#pragma mark - View Lifecycle

/**
 *	Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	//	configure navigation controller
	self.activityNavigationController.delegate = self.activityNavigationDelegate;
	self.activityNavigationController.navigationBarHidden = YES;
	
	//	adopt navigation controller
	[self ej_adoptViewControllerWithAutoLayout:self.activityNavigationController];
	
	//	set up data source for activity items view
	self.activityNavigationDataSource = [[ActivityNavigationDataSource alloc] initWithActivityItemsView:self.activityItemsCollectionView
																				  andRootNavigationItem:self.activityNavigationRootItem];
	
	//	set up the navigation item to be used in each view controller
	self.activityNavigationItem = [[ActivityNavigationItem alloc] initWithDataSource:self.activityNavigationDataSource];
	self.rootViewController.activityNavigationItem = self.activityNavigationItem;
	
	[self addConstraints];
	
	[self configureView];
}

@end