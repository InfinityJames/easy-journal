//
//  ActivitySelectionViewController.m
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivitySelectionViewController.h"

#pragma mark - Activity Selection View Controller Private Class Extension

@interface ActivitySelectionViewController () <EasyJournalAnimatableProtocol> {}

#pragma mark - Private Properties: State

/**	Whether or not the thumbnails of activities should be hidden in the table view.	*/
@property (nonatomic, assign)				BOOL				shouldHideThumbnails;

#pragma mark - Private Properties: Subviews

/**	The table view that holds the current selections.	*/
@property (nonatomic, strong, readwrite)	UITableView			*selectionsTableView;

@end

#pragma mark - Activity Selection View Controller Implementation

@implementation ActivitySelectionViewController {}

#pragma mark - ArrayDataSourceDelegate Methods

/**
 *	Used to configure a cell with a given item.
 *
 *	@param	arrayDataSource				The array data source calling for this configuration.
 *	@param	cell						The table view / collection view cell requiring configuration.
 *	@param	item						The item associated with the given cell.
 *	@param	indexPath					The index path of the given cell.
 */
- (void)arrayDataSource:(ArrayDataSource *)arrayDataSource
		  configureCell:(id)cell
			   withItem:(id)item
			atIndexPath:(NSIndexPath *)indexPath
{
	ActivityCell *activityCell			= cell;
	
	//	if an activity we don't want to show the images
	if (self.shouldHideThumbnails)
		activityCell.activityThumbnailView.hidden	= YES;
	else
		activityCell.activityThumbnailView.hidden	= NO;
}

#pragma mark - EasyJournalAnimatableProtocol Methods

/**
 *	Requesting the activity object which relates to a given view.
 *
 *	@param	view						The view for which we need the appropriate activity object.
 *
 *	@return	An NSObject conforming to the ActivityProtocol which relates to the passed UIView.
 */
- (id <ActivityProtocol>)activityForView:(UIView *)view
{
	id <ActivityProtocol> activityForView;
	UIImageView *activityView = (UIImageView *)view;
	
	for (id <ActivityProtocol> activity in self.activities)
		if ([activityView.image isEqual:[activity activityImage]])
			activityForView = activity;
	
	return activityForView;
}

/**
 *	Requesting the destination frame for a given view.
 *
 *	@return	A CGRect detailing the desired frame for the given view by the end of the animation.
 */
- (CGRect)destinationFrameForView:(UIView *)view
{
	return [self.activityNavigationController frameForNextActivityRelativeToView:self.view.superview];
}

/**
 *	An array of views to dismiss during the animation.
 *
 *	@return	An NSArray of UIViews to de dismissed during the animation.
 */
- (NSArray *)viewsToDismiss
{
	//	get the index paths of the cells whose views we dismiss and add their views
	NSMutableArray *dismissableViews	= [[NSMutableArray alloc] initWithCapacity:self.selectionsTableView.indexPathsForVisibleRows.count];
	for (NSIndexPath *dismissableIndexPath in self.selectionsTableView.indexPathsForVisibleRows)
		if (![dismissableIndexPath isEqual:self.selectedIndexPath])
		{
			ActivityCell *activityCell	= (ActivityCell *)[self.selectionsTableView cellForRowAtIndexPath:dismissableIndexPath];
			UIImageView *viewToDismiss	= [activityCell.activityThumbnailView jv_imageViewCopy];
			//	we transform the frame to this view's co-ordinates
			viewToDismiss.frame			= [activityCell convertRect:viewToDismiss.frame toView:self.view];
			[dismissableViews addObject:viewToDismiss];
		}
	
	return dismissableViews;
}

/**
 *	The view to keep through the animation.
 *
 *	@return	A UIView to keep throughout the transition animation.
 */
- (UIView *)viewToKeep
{
	if (!self.selectedIndexPath)	return nil;
	
	ActivityCell *activityCell		= (ActivityCell *)[self.selectionsTableView cellForRowAtIndexPath:self.selectedIndexPath];
	
	if (!activityCell)
		activityCell				= [[ActivityCell alloc] initWithTitle:@"" image:self.selectedActivity.activityImage];
	
	//	we transform the frame to this view's co-ordinates
	UIView *viewToKeep				= [activityCell.activityThumbnailView jv_imageViewCopy];
	viewToKeep.frame				= [activityCell convertRect:viewToKeep.frame toView:self.view];
	
	return viewToKeep;
}

#pragma mark - Initialisation

/**
 *	Initialises this activity selection view controller with the activities that can be selected.
 *
 *	@param	activities					An array of objects that conform to the ActivityProtocol.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithActivities:(NSArray *)activities
{
	if (self = [super init])
	{
		_activities						= activities;
	}
	
	return self;
}

#pragma mark - Property Accessor Methods - Getters

/**
 *	The table view that holds the current selections.
 *
 *	@return	The table view that holds the current selections.
 */
- (UITableView *)selectionsTableView
{
	if (!_selectionsTableView)
	{
		_selectionsTableView				= [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
		_selectionsTableView.allowsSelection= YES;
		_selectionsTableView.delegate		= self;
		_selectionsTableView.separatorColor	= [UIColor clearColor];
		_selectionsTableView.separatorStyle	= UITableViewCellSeparatorStyleNone;
		
		[_selectionsTableView registerClass:[ActivityCell class] forCellReuseIdentifier:self.cellIdentifier];
		
		[self.view addSubviewForAutoLayout:_selectionsTableView];
	}
	
	return _selectionsTableView;
}

#pragma mark - Property Accessor Methods - Setters

/**
 *	The data source to be used for the UITableView.
 *
 *	@param	The data source to be used for the UITableView.
 */
- (void)setArrayDataSource:(ArrayDataSource *)arrayDataSource
{
	if (_arrayDataSource == arrayDataSource)
		return;
	
	_arrayDataSource					= arrayDataSource;
	
	self.selectionsTableView.dataSource	= _arrayDataSource;
}

/**
 *	The table view that holds the current selections.
 *
 *	@param	selectedActivity			The table view that holds the current selections.
 */
- (void)setSelectedActivity:(id<ActivityProtocol>)selectedActivity
{
	if ([_selectedActivity isEqual:selectedActivity])
		return;
	
	_selectedActivity					= selectedActivity;
	
	if (!_selectedActivity)
		self.shouldHideThumbnails		= NO;
	else
		self.shouldHideThumbnails		= YES;
}

#pragma mark - Subview Management

/**
 *	Add the constraints for the subviews.
 */
- (void)addConstraints
{
	NSDictionary *viewsDictionary		= @{@"tableView"	: self.selectionsTableView,
											@"top"			: self.topLayoutGuide		};
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|"
																	  options:kNilOptions
																	  metrics:nil
																		views:viewsDictionary]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|"
																	  options:kNilOptions
																	  metrics:nil
																		views:viewsDictionary]];
}

#pragma mark - UITableViewDelegate Methods

/**
 *	Tells the delegate that the specified row is now selected.
 *
 *	@param	tableView					A table-view object informing the delegate about the new row selection.
 *	@param	indexPath					An index path locating the new selected row in tableView.
 */
- (void)	  tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.selectedIndexPath				= indexPath;
	self.selectedActivity				= [self.arrayDataSource itemForRowAtIndexPath:indexPath];
	
	[self.selectionsTableView reloadRowsAtIndexPaths:self.selectionsTableView.indexPathsForVisibleRows
									withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - View Lifecycle

/**
 *	Notifies the view controller that its view was removed from a view hierarchy.
 *
 *	@param	animated					If YES, the disappearance of the view was animated.
 */
- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	self.selectedActivity				= nil;
	self.selectedIndexPath				= nil;
}

/**
 *	Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self addConstraints];
	
	self.arrayDataSource				= [[ArrayDataSource alloc] initWithItems:self.activities
												  cellIdentifier:self.cellIdentifier
													 andDelegate:self];
}

/**
 *	Notifies the view controller that its view is about to be added to a view hierarchy.
 *
 *	@param	animated					If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self.selectionsTableView reloadData];
}

/**
 *	Notifies the view controller that its view is about to be removed from a view hierarchy.
 *
 *	@param	animated					If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	self.shouldHideThumbnails			= YES;
	
	
	[self.selectionsTableView reloadRowsAtIndexPaths:self.selectionsTableView.indexPathsForVisibleRows
									withRowAnimation:UITableViewRowAnimationNone];
}

@end