//
//  EasyJournalTransitionAnimationController.h
//  EasyJournal
//
//  Created by James Valaitis on 05/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

@protocol ActivityProtocol;

#pragma mark - Constants & Static Variables

/**	Called once an animation transition has successfully completed.	*/

typedef void(^EasyJournalAnimationCompletion)(id <ActivityProtocol> activity);

#pragma mark - #pragma mark - Easy Journal Transition Animation Controller Private Class Extension Public Interface

@interface EasyJournalTransitionAnimationController : NSObject <UIViewControllerAnimatedTransitioning> {}

#pragma mark - Public Properties

/**	Called upon completion of an animated transition.	*/
@property (nonatomic, copy)		EasyJournalAnimationCompletion		completionHandler;
/**	Whether or not this should be a pop transition, or a standard push transition.	*/
@property (nonatomic, assign)	BOOL								reverse;

#pragma mark - Public Methods

/**
 *	Initialise an animation controller with a block to call upon completion.
 *
 *	@param	completion					The block to call upon the completion of any transition controlled.
 *
 *	@return	An initialized object.
 */
- (instancetype)initWithCompletionHandler:(EasyJournalAnimationCompletion)completion;

@end