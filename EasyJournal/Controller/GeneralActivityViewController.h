//
//  GeneralActivityViewController.h
//  EasyJournal
//
//  Created by James Valaitis on 07/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivitySelectionViewController.h"

#pragma mark - General Activity View Controller Public Interface

@interface GeneralActivityViewController : ActivitySelectionViewController {}

@end