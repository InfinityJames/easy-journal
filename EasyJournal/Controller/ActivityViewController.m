//
//  ActivityViewController.m
//  EasyJournal
//
//  Created by James Valaitis on 09/10/2013.
//  Copyright (c) 2013 &Beyond. All rights reserved.
//

#import "ActivityViewController.h"

#pragma mark - Activity View Controller Private Class Extension

@interface ActivityViewController () {}

@end

#pragma mark - Activity View Controller Implementation

@implementation ActivityViewController {}

@end